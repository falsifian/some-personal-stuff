-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module AllTests where

import {-@ HTF_TESTS @-} NineMensMorris.CanonicalizedGameStateTest hiding (main)
import {-@ HTF_TESTS @-} NineMensMorris.GameStateTest hiding (main)
import {-@ HTF_TESTS @-} NineMensMorris.LocationTest hiding (main)
import {-@ HTF_TESTS @-} NineMensMorris.MillTest hiding (main)
import {-@ HTF_TESTS @-} NineMensMorris.SuccessorStatesTest hiding (main)
import {-@ HTF_TESTS @-} Game.SolveTest hiding (main)
import Test.Framework

main :: IO ()
main = htfMain htf_importedTests

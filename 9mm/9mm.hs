{-# LANGUAGE NamedFieldPuns #-}

module Main where

import Control.Monad
import Data.IORef
import qualified Data.Time.Clock as Clock
import qualified Data.Time.Clock.POSIX as PosixClock
import qualified GHC.Stats
import Game.Game
import Game.Solve (solveGameFromPosition)
import qualified NineMensMorris.GameOptions
import NineMensMorris.CanonicalizedGameState (CanonicalizedState, successorStates, start)
import System.Exit
import Text.Printf

game :: Game CanonicalizedState
game =
  Game
  { successors =
       successorStates ( NineMensMorris.GameOptions.Options
                         { NineMensMorris.GameOptions.flying = False
                         }
                       )
  }

updateIntervalSeconds = 5
maxSteps = 1/0  -- infinity

updateStatus :: IORef Integer -> IORef Clock.UTCTime -> String -> IO ()
updateStatus i time status =
  do iv <- readIORef i
     previousTime <- readIORef time
     currentTime <- Clock.getCurrentTime
     when (Clock.diffUTCTime currentTime previousTime > updateIntervalSeconds) $
       do writeIORef time currentTime
          gcstats <- GHC.Stats.getGCStats
          printf "%d: time %s\nbytes used %d, slop %d\n%s\n" iv (show currentTime) (GHC.Stats.currentBytesUsed gcstats) (GHC.Stats.currentBytesSlop gcstats) status
     when (fromIntegral iv > maxSteps) exitSuccess
     writeIORef i (iv + 1)

main :: IO ()
main =
  do i <- newIORef 0
     time <- newIORef (PosixClock.posixSecondsToUTCTime 0)
     outcome <- solveGameFromPosition game start
                (updateStatus i time)
     print outcome

module Game.Game
( Game(Game, successors)
) where

data Game position =
  Game
  { -- If there are no successors, the current player has lost.
    successors :: position -> [position]
  }

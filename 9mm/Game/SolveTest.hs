-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module Game.SolveTest where

import qualified Control.Monad
import Control.Monad.Identity (runIdentity)
import qualified Data.Map as Map
import Game.Game
import Game.Solve
import Test.Framework

data TreeGame = Node {children :: [TreeGame]} deriving (Eq, Ord, Show)

simpleSolve :: (Ord position, Show position) =>
               Game position -> position -> Outcome
simpleSolve game position =
  runIdentity $ solveGameFromPosition game position (\_ -> return ())

test_solveTreeGames :: IO ()
test_solveTreeGames =
  let game = Game {successors = children}
  in do assertEqual PrevWins $ simpleSolve game (Node [])
        assertEqual NextWins $ simpleSolve game (Node [Node []])
        assertEqual NextWins $
          simpleSolve game
          ( Node
            [ Node []
            , Node [Node []]
            ]
          )
        assertEqual PrevWins $
          simpleSolve game
          ( Node
            [ Node [Node []]
            , Node [Node [], Node [Node []]]
            ]
          )

data GraphGame =
  GraphGame
  { edges :: Map.Map Int [Int]
  , currentNode :: Int
  } deriving (Eq, Ord, Show)

-- These can have draws.
test_solveGraphGames :: IO ()
test_solveGraphGames =
  let game =
        Game
        { successors = \ position ->
             [ position {currentNode = n}
             | n <- Map.findWithDefault [] (currentNode position)
                    (edges position)
             ]
        }
  in do assertEqual Draw $
          simpleSolve game
          (GraphGame (Map.fromList [(42, [42])]) 42)
        assertEqual PrevWins $
          simpleSolve game
          (GraphGame (Map.fromList [(42, [42])]) 0)
        assertEqual NextWins $
          simpleSolve game
          (GraphGame (Map.fromList [(0, [0, 1])]) 0)
        assertEqual Draw $
          simpleSolve game
          (GraphGame (Map.fromList [(0, [0, 1]), (1, [2])]) 0)
        assertEqual PrevWins $
          simpleSolve game
          (GraphGame (Map.fromList [(0, [1]), (1, [2, 4]), (2, [3]), (3, [2, 0])]) 0)
        assertEqual NextWins $
          simpleSolve game
          ( GraphGame ( Map.fromList [ (0, [1, 4])
                                     , (1, [2])
                                     , (2, [3])
                                     , (3, [4, -1])
                                     , (4, [5])
                                     , (5, [4, 2])
                                     ]
                      )
                      0
          )

data StepCounterMonad a = CountingSteps [()] a

instance Functor StepCounterMonad where
  fmap = Control.Monad.liftM

instance Applicative StepCounterMonad where
  pure = CountingSteps []
  (<*>) = Control.Monad.ap

instance Monad StepCounterMonad where
  CountingSteps steps0 x >>= f =
    let CountingSteps steps1 y = f x in
    CountingSteps (steps0 ++ steps1) y

-- A game with infinitely many states that are reachable from the starting
-- position, but where it's possible to see the starting position is a win by
-- examining just two nodes.
test_solveInfiniteGame :: IO ()
test_solveInfiniteGame =
  let maxSteps = 100
      successors 0 = [1, 2]
      successors 1 = []
      successors n | n > 1 = [n + 1]
      CountingSteps steps solution =
                       solveGameFromPosition
                       (Game {successors = successors})
                       0
                       (\_ -> CountingSteps [()] ())
      answer = if length (take (maxSteps + 1) steps) < maxSteps + 1 then Just solution else Nothing
  in assertEqual (Just NextWins) answer

main :: IO ()
main = htfMain htf_thisModulesTests

{-# LANGUAGE ScopedTypeVariables #-}

module Game.Solve
( Outcome(..)
, solveGameFromPosition
) where

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Game.Game (Game(Game, successors))
import Text.Printf (printf)

data Outcome = NextWins | PrevWins | Draw deriving (Eq, Show)

data Winner = Next | Prev deriving (Eq, Ord)

opposite :: Winner -> Winner
opposite Next = Prev
opposite Prev = Next

data SolverState position =
  SolverState
  { knownPositions :: Map.Map (position, Winner) Bool
  , checksInProgress :: Set.Set (position, Winner)
  }

initialSolverState :: SolverState position
initialSolverState =
  SolverState
  { knownPositions = Map.empty
  , checksInProgress = Set.empty
  }

insertKnownPosition :: Ord position =>
                       position -> Winner -> Bool ->
                       Map.Map (position, Winner) Bool ->
                       Map.Map (position, Winner) Bool
insertKnownPosition position winner win knownPositions =
  let knownPositions' = Map.insert (position, winner) win knownPositions
  in if win
     then Map.insert (position, opposite winner) False knownPositions'
     else knownPositions'

-- Given a game and a starting position, determines the outcome of the
-- game. Runs updateStatus with some information about the solver's state every
-- time a position is examined.
solveGameFromPosition ::
  forall position m.
  (Ord position, Show position, Monad m) =>
  Game position -> position -> (String -> m ()) -> m Outcome
solveGameFromPosition game startingPosition updateStatus =
  let checkOutcome :: SolverState position -> Winner -> position ->
                      m (Bool, SolverState position)
      checkOutcome solverState winnerToCheck positionToCheck
        | Just answer <- Map.lookup
                         (positionToCheck, winnerToCheck)
                         (knownPositions solverState)
          = return (answer, solverState)
        | Set.member (positionToCheck, winnerToCheck) (checksInProgress solverState)
          = return (False, solverState)
        | otherwise =
             let solverState' = solverState
                                { checksInProgress = Set.insert
                                                     (positionToCheck, winnerToCheck)
                                                     (checksInProgress solverState)
                                }
             in do updateStatus $
                     printf "exploring %s\nmap size %d\ncurrent depth %d\n"
                     (show positionToCheck)
                     (Map.size (knownPositions solverState))
                     (Set.size (checksInProgress solverState))
                   (answer, solverState'') <- checkOutcomeUsingSuccessors
                                              solverState'
                                              winnerToCheck
                                              (successors game positionToCheck)
                   return ( answer
                          , SolverState { knownPositions =
                                             insertKnownPosition
                                             positionToCheck
                                             winnerToCheck
                                             answer
                                             (knownPositions solverState'')
                                        , checksInProgress =
                                             -- We could simply write
                                             -- checksInProgress solverState
                                             -- here, but that would force the
                                             -- garbage collecter to hang on to
                                             -- it at every level of the call
                                             -- stack, so we re-compute it
                                             -- instead.
                                             Set.delete
                                             (positionToCheck, winnerToCheck)
                                             (checksInProgress solverState'')
                                        }
                          )
      checkOutcomeUsingSuccessors :: SolverState position -> Winner -> [position] ->
                                     m (Bool, SolverState position)
      checkOutcomeUsingSuccessors solverState winnerToCheck [] =
        return (Prev == winnerToCheck, solverState)
      checkOutcomeUsingSuccessors solverState winnerToCheck (h : t) =
        do (hWins, solverState') <- checkOutcome
                                    solverState
                                    (opposite winnerToCheck)
                                    h
           if (Next == winnerToCheck) == hWins
             then return (hWins, solverState')
             else checkOutcomeUsingSuccessors solverState' winnerToCheck t
  in do (nextWins, solverState) <- checkOutcome
                                   initialSolverState
                                   Next
                                   startingPosition
        if nextWins
          then return NextWins
          else do (prevWins, _solverState') <- checkOutcome
                                               solverState
                                               Prev
                                               startingPosition
                  return $ if prevWins then PrevWins else Draw

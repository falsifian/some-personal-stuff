{-# LANGUAGE ScopedTypeVariables #-}

-- This version is buggy. It fails SolveTest.hs.

module Game.Solve
( Outcome(..)
, solveGameFromPosition
) where

import qualified Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Game.Game
import Text.Printf (printf)

data Outcome = NextWins | PrevWins | Draw deriving (Eq, Show)

data PartialOutcome position =
  Known !Outcome | DrawIfAnyOfTheseAre !(Set.Set position)
  deriving Eq

updatePartialKnowledge :: Ord position =>
  Maybe (PartialOutcome position) -> PartialOutcome position ->
  PartialOutcome position
updatePartialKnowledge Nothing x = x
updatePartialKnowledge _ x@(Known _) = x
updatePartialKnowledge (Just x@(Known _)) _ = x
updatePartialKnowledge (Just (DrawIfAnyOfTheseAre x)) (DrawIfAnyOfTheseAre y) =
  DrawIfAnyOfTheseAre (Set.union x y)

updateBestWithOpponentOutcome :: Ord position =>
  PartialOutcome position -> PartialOutcome position -> PartialOutcome position
updateBestWithOpponentOutcome x@(Known NextWins) _ = x
updateBestWithOpponentOutcome x@(Known Draw) y
  | y == Known PrevWins = Known NextWins
  | otherwise = x
updateBestWithOpponentOutcome (Known PrevWins) (Known NextWins) = Known PrevWins
updateBestWithOpponentOutcome (Known PrevWins) (Known Draw) = Known Draw
updateBestWithOpponentOutcome (Known PrevWins) (Known PrevWins) = Known NextWins
updateBestWithOpponentOutcome (Known PrevWins) x@(DrawIfAnyOfTheseAre _) = x
updateBestWithOpponentOutcome x@(DrawIfAnyOfTheseAre _) (Known NextWins) = x
updateBestWithOpponentOutcome x@(DrawIfAnyOfTheseAre _) (Known Draw) =
  Known Draw
updateBestWithOpponentOutcome x@(DrawIfAnyOfTheseAre _) (Known PrevWins) =
  Known NextWins
updateBestWithOpponentOutcome (DrawIfAnyOfTheseAre x) (DrawIfAnyOfTheseAre y) =
  DrawIfAnyOfTheseAre (Set.union x y)

-- Given a game and a starting position, determines the outcome of the
-- game. Runs updateStatus with some information about the solver's state every
-- time a position is examined.
solveGameFromPosition ::
  forall position m.
  (Ord position, Show position, Monad m) =>
  Game position -> position -> (String -> m ()) -> m Outcome
solveGameFromPosition game start updateStatus =
  let solveWith ::
        -- The returned Set.Set position will be equal to the assumedDraws
        -- argument. This return value exists to enable a memory-saving trick:
        -- with a deep stack of many solveWith / solveUsingSuccessors calls, we
        -- can avoid storing a copy of assumedDraws at each level of the stack
        -- by reconstructing the parent value from the child value.
        Map.Map position (PartialOutcome position) -> Set.Set position ->
        position ->
        m (PartialOutcome position, Map.Map position (PartialOutcome position), Set.Set position)
      solveWith knowledge assumedDraws pos =
        let posSuccessors = successors game pos in
        case Map.lookup pos knowledge of
          Just x@(Known _) -> return (x, knowledge, assumedDraws)
          Just x@(DrawIfAnyOfTheseAre positions)
            | not (Set.null (Set.intersection assumedDraws positions)) ->
              return (x, knowledge, assumedDraws)
          partialKnowledge | Set.member pos assumedDraws ->
            let partialOutcome = updatePartialKnowledge partialKnowledge
                                 (DrawIfAnyOfTheseAre (Set.singleton pos))
            in return (partialOutcome, Map.insert pos partialOutcome knowledge, assumedDraws)
          -- TODO: Replace this hack with a smarter way to explore the game
          -- tree, e.g. breadth first.
          partialKnowledge
            | or [ Map.lookup position knowledge == Just (Known PrevWins)
                 | position <- posSuccessors]
            -> return (Known NextWins, Map.insert pos (Known NextWins) knowledge, assumedDraws)
          partialKnowledge ->
            do updateStatus $
                 printf "exploring %s\nmap size %d\n# assumed draws: %d\n"
                 (show pos) (Map.size knowledge) (Set.size assumedDraws)
               -- At this point, pos is not a member of assumedDraws, so we can
               -- reconstruct assumedDraws from (Set.insert pos assumedDraws).
               (partialOutcome, knowledge', assumedDraws') <-
                  solveUsingSuccessors
                  knowledge (Set.insert pos assumedDraws) posSuccessors
                  (Known PrevWins)
               partialOutcome' <- return
                 ( case partialOutcome of
                      DrawIfAnyOfTheseAre x | Set.member pos x -> Known Draw
                      y -> y
                 )
               return ( partialOutcome'
                      , Map.insert pos
                        ( updatePartialKnowledge partialKnowledge
                          partialOutcome'
                        )
                        knowledge'
                      , Set.delete pos assumedDraws'
                      )
      solveUsingSuccessors ::
        Map.Map position (PartialOutcome position) -> Set.Set position ->
        [position] -> PartialOutcome position ->
        m (PartialOutcome position, Map.Map position (PartialOutcome position), Set.Set position)
      solveUsingSuccessors knowledge assumedDraws [] bestSoFar =
        return (bestSoFar, knowledge, assumedDraws)
      solveUsingSuccessors knowledge assumedDraws (_:_) (Known NextWins) =
        return (Known NextWins, knowledge, assumedDraws)
      solveUsingSuccessors knowledge assumedDraws (h : t) bestSoFar =
        do (hOutcome, knowledge', assumedDraws') <- solveWith knowledge assumedDraws h
           let bestSoFar' = updateBestWithOpponentOutcome bestSoFar hOutcome
           solveUsingSuccessors knowledge' assumedDraws' t bestSoFar'
  in do (Known finalOutcome, _, _) <- solveWith (Map.empty) (Set.empty) start
        return finalOutcome

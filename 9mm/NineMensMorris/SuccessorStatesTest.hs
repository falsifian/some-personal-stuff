-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module NineMensMorris.SuccessorStatesTest where

import NineMensMorris.GameOptions
import qualified NineMensMorris.GameState as GameState
import qualified NineMensMorris.GameState
import qualified NineMensMorris.Location as Location
import NineMensMorris.SuccessorStates
import Test.Framework

assertFalse = assertBool . not

test_allSuccessorStatesOneExample :: IO ()
test_allSuccessorStatesOneExample =
  let r s = GameState.buildGameState (read s) 8
  in assertListsEqualAsSets
       [ -- capturing moves
         r "_wbbb___'www_____'_w_____b"
       , r "_wbbb__b'www_____'_w______"
         -- non-capturing moves
       , r "wwbbb__b'_ww_____'_w_____b"
       , r "_wbbbw_b'_ww_____'_w_____b"
       , r "_wbbb_wb'_ww_____'_w_____b"
       , r "_wbbb__b'_www____'_w_____b"
       , r "_wbbb__b'_ww_w___'_w_____b"
       , r "_wbbb__b'_ww__w__'_w_____b"
       , r "_wbbb__b'_ww___w_'_w_____b"
       , r "_wbbb__b'_ww____w'_w_____b"
       , r "_wbbb__b'_ww_____'ww_____b"
       , r "_wbbb__b'_ww_____'_ww____b"
       , r "_wbbb__b'_ww_____'_w_w___b"
       , r "_wbbb__b'_ww_____'_w__w__b"
       , r "_wbbb__b'_ww_____'_w___w_b"
       , r "_wbbb__b'_ww_____'_w____wb"
       ]
       ( successorStates
         (Options {flying = True})
         ( GameState.buildGameState (read "_bwww__w'_bb_____'_b_____w") 9
         )
       )

test_successorsAtStart :: IO ()
test_successorsAtStart =
  assertListsEqualAsSets
    [ GameState.buildGameState
      (GameState.makeBoard [(location, GameState.White)])
      17
    | location <- Location.allLocations
    ]
  (successorStates (Options {flying = False}) GameState.start)

test_captureFromMill :: IO ()
test_captureFromMill =
  assertBool
  ( elem
    ( GameState.buildGameState (read "bb_bb___'www_____'www_____") 6
    )
    ( successorStates
      (Options {flying = False})
      ( GameState.buildGameState (read "wwwww___'bb______'bbb_____") 7
      )
    )
  )

test_someFlyingAndSlidingMoves :: IO ()
test_someFlyingAndSlidingMoves =
  let state = ( GameState.buildGameState (read "bbb_____'_______w'wwwwwwww") 0
              )
  in do assertEqual
          (12 * 3)
          (length (successorStates (Options {flying = True}) state))
        assertEqual
          3
          (length (successorStates (Options {flying = False}) state))
        assertBool
          ( elem
            ( GameState.buildGameState (read "_ww_____'______wb'bbbbbbbb") 0
            )
            (successorStates (Options {flying = True}) state)
          )

test_noFlyingWhilePlacing :: IO ()
test_noFlyingWhilePlacing =
  assertFalse
    ( elem
      ( GameState.buildGameState (read "_ww_____'______w_'_____bbb") 12
      )
      ( successorStates
        (Options {flying = True})
        ( GameState.buildGameState (read "bbb_____'________'_____www") 12
        )
      )
    )

test_win :: IO ()
test_win =
  do assertEqual
       []
       ( successorStates
         (Options {flying = True})
         ( GameState.buildGameState (read "bb______'________'_____www") 0
         )
       )
     assertEqual
       []
       ( successorStates
         (Options {flying = False})
         ( GameState.buildGameState (read "bbbw___w'_w______'________") 0
         )
       )

main :: IO ()
main = htfMain htf_thisModulesTests

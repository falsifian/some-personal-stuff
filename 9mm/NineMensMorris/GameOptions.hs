module NineMensMorris.GameOptions where

data Options =
  Options
  { -- If true, a player with only three pieces left can move a piece from
    -- anywhere to anywhere each turn.
    flying :: Bool
  }

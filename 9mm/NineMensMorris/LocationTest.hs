-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module NineMensMorris.LocationTest where

import qualified Data.Set as Set
import NineMensMorris.Location
import Prelude hiding (cycle)
import Test.Framework

test_polarWrapsAround :: IO ()
test_polarWrapsAround =
  do assertEqual (polar 0 7) $ polar 0 (-1)
     assertEqual (polar 1 1) $ polar 1 17
     assertEqual (polar 2 3) $ polar 2 (-5)

test_adjacentLocationsCorner :: IO ()
test_adjacentLocationsCorner =
  do assertListsEqualAsSets  -- adjacent to ring 0 corner
       [polar 0 1, polar 0 7]
       (adjacentLocations (polar 0 0))
     assertListsEqualAsSets  -- adjacent to ring 1 corner
       [polar 1 3, polar 1 5]
       (adjacentLocations (polar 1 4))

test_adjacentLocationsEdge :: IO ()
test_adjacentLocationsEdge =
  do assertListsEqualAsSets  -- four adjacent nodes
       [ polar 0 3
       , polar 2 3
       , polar 1 2
       , polar 1 4
       ]
       (adjacentLocations (polar 1 3))
     assertListsEqualAsSets  -- adjacent to ring 2 middle-of-edge node
       [ polar 2 4
       , polar 2 6
       , polar 1 5
       ]
       (adjacentLocations (polar 2 5))

test_allLocationsLength :: IO ()
test_allLocationsLength = assertEqual 24 $ length allLocations

test_allLocationsDistinct :: IO ()
test_allLocationsDistinct =
  do assertBool $
       Set.size (Set.fromList allLocations) == length allLocations

test_allPolarLocationsValid :: IO ()
test_allPolarLocationsValid =
  assertBool $
  and [ x >= 0 && x <= 2 && y >= 0 && y <= 7
      | (x, y) <- map toPolar allLocations
      ]

test_polarRejectsBadInput :: IO ()
test_polarRejectsBadInput =
  do assertThrows (polar (-1) 3) (== BadCoordinatesException)
     assertThrows (polar 3 3) (== BadCoordinatesException)
     assertThrows (polar 4 9) (== BadCoordinatesException)

test_toIntIsFrom0To23 :: IO ()
test_toIntIsFrom0To23 =
  do assertBool $ and [ x >= 0 && x <= 23
                      | l <- allLocations
                      , let x = toInt l
                      ]

test_encodeAsInt :: IO ()
test_encodeAsInt =
  do assertBool $ and [ l == fromInt (toInt l)
                      | l <- allLocations
                      ]

main :: IO ()
main = htfMain htf_thisModulesTests

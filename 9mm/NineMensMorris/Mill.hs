module NineMensMorris.Mill
( inBlackMill
) where

import qualified Data.Map as Map
import qualified NineMensMorris.GameState as GameState
import qualified NineMensMorris.Location as Location
import NineMensMorris.Location (Location, polar, toPolar)

inBlackMill :: Location -> GameState.Board -> Bool
inBlackMill location board =
    let (ring, fromTopLeft) = toPolar location
        setsToCheck =
          if mod fromTopLeft 2 == 1
          then 
            [ -- Mill across rings
              [polar ring' fromTopLeft | ring' <- [0..2]]
            , -- Mill within ring
              [ polar ring fromTopLeft'
              | fromTopLeft' <- [fromTopLeft + 1, fromTopLeft - 1]
              ]
            ]
          else
            -- Two mills within the ring
            [ [ polar ring fromTopLeft'
              | fromTopLeft' <- [fromTopLeft + 1 , fromTopLeft + 2]
              ]
            , [ polar ring fromTopLeft'
              | fromTopLeft' <- [fromTopLeft - 1 , fromTopLeft - 2]
              ]
            ]
    in or [ and [ GameState.pieceAt location board == Just GameState.Black
                | location <- set
                ]
          | set <- setsToCheck
          ]

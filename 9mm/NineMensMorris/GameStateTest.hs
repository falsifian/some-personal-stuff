-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module NineMensMorris.GameStateTest where

import qualified Data.Map.Strict as Map
import NineMensMorris.GameState
import qualified NineMensMorris.Location as Location
import NineMensMorris.Location (polar)
import Test.Framework

test_startingBlackLocations :: IO ()
test_startingBlackLocations = assertEqual [] (blackLocations (board start))

test_blackLocations :: IO ()
test_blackLocations =
  do assertEqual
       [polar 1 3]
       (blackLocations (makeBoard [(polar 1 3, Black)]))
     assertEqual
       []
       ( blackLocations $ makeBoard $
         [ (polar 2 3, White)
         , (polar 2 4, White)
         , (polar 0 0, White)
         ]
       )
     assertListsEqualAsSets
       [polar 1 7, polar 2 1]
       ( blackLocations $ makeBoard $
         [ (polar 2 3, White)
         , (polar 1 7, Black)
         , (polar 0 0, White)
         , (polar 2 1, Black)
         ]
       )

test_switchPlayers :: IO ()
test_switchPlayers =
  do assertEqual
       (makeBoard [])
       (switchPlayers (makeBoard []))
     assertEqual
       (makeBoard [(polar 1 2, White), (polar 2 3, Black)])
       ( switchPlayers
         (makeBoard [(polar 1 2, Black), (polar 2 3, White)])
       )
     assertEqual
       (makeBoard [(polar 1 3, White), (polar 2 2, Black), (polar 0 1, Black)])
       ( switchPlayers
         (makeBoard [(polar 1 3, Black), (polar 0 1, White), (polar 2 2, White)])
       )

boardToMap :: Board -> Map.Map Location.Location Player
boardToMap board =
  Map.fromList [ (l, p)
               | l <- Location.allLocations
               , Just p <- return $ pieceAt l board
               ]

test_makeEmptyBoard :: IO ()
test_makeEmptyBoard = assertEqual Map.empty (boardToMap (makeBoard []))

test_makeBoard :: IO ()
test_makeBoard =
  do assertEqual
       (Map.singleton (polar 2 2) White)
       (boardToMap (makeBoard [(polar 2 2, White)]))
     assertEqual
       ( Map.fromList
         [ (polar 1 2, White)
         , (polar 0 6, Black)
         , (polar 1 1, White)
         ]
       )
       ( boardToMap $
         makeBoard [ (polar 1 2, White)
                   , (polar 0 6, Black)
                   , (polar 1 1, White)
                   ]
       )

test_makeBoardRejectsDuplicates :: IO ()
test_makeBoardRejectsDuplicates =
  do assertThrows
       (makeBoard [(polar 1 2, Black), (polar 1 2, Black)])
       (== DuplicateLocationException)
     assertThrows
       (makeBoard [(polar 1 3, White), (polar 2 4, Black), (polar 1 3, Black)])
       (== DuplicateLocationException)

test_readBoard :: IO ()
test_readBoard =
  do assertEqual
       (makeBoard [])
       (read "________'________'________")
     assertEqual
       ( makeBoard [ (polar 0 0, Black)
                   , (polar 0 7, White)
                   , (polar 2 2, White)
                   ]
       )
       (read "b______w'________'__w_____")
     assertEqual
       (makeBoard [(l, White) | l <- Location.allLocations])
       (read "wwwwwwww'wwwwwwww'wwwwwwww")

test_unreadableBoard :: IO ()
test_unreadableBoard =
  do assertEqual [] (readsPrec 0 "" :: [(Board, String)])
     -- Invalid character 'x'.
     assertEqual []
       (readsPrec 0 "x_______'________'________" :: [(Board, String)])
     -- Too many spaces in ring 0.
     assertEqual []
       (readsPrec 0 "_________'________'________" :: [(Board, String)])

test_readBoardExtra :: IO ()
test_readBoardExtra =
  assertEqual [(makeBoard [], " 5")]
  (readsPrec 0 "________'________'________ 5")

test_showBoardInverseOfRead :: IO ()
test_showBoardInverseOfRead =
  sequence_
  [ assertEqual s (show (read s :: Board))
  | s <- [ "________'________'________"
         , "__b_____'____w___'________"
         , "________'_____www'________"
         , "bbbbbbbb'bbbbwbbb'wwwwwwww"
         , "bbbbbbbb'bbbbbbbb'bbbbbbbb"
         , "wwwwwwww'wwwwwwww'wwwwwwww"
         , "b_w__bbb'_w_w_wwb'b___b_ww"
         ]
  ]

test_pieceAt :: IO ()
test_pieceAt =
  let board = read "_bw_bw_b'wwwwbwbw'w_w_bb__"
      cases = [ (polar 0 0, Nothing)
              , (polar 0 7, Just Black)
              , (polar 1 0, Just White)
              , (polar 1 2, Just White)
              , (polar 1 4, Just Black)
              , (polar 2 1, Nothing)
              , (polar 2 5, Just Black)
              ]
  in sequence_ [assertEqual b (pieceAt a board) | (a, b) <- cases]

test_change :: IO ()
test_change =
  do assertEqual
       (read "________'__b_____'________")
       (change (polar 1 2) (Just Black) (makeBoard []))
     assertEqual
       (makeBoard [])
       (change (polar 1 2) Nothing (read "________'__b_____'________"))
     assertEqual
       (read "_b__w___'_bbww___'bbww_b__")
       (change (polar 2 0) (Just Black) (read "_b__w___'_bbww___'wbww_b__"))

test_numBlackPieces :: IO ()
test_numBlackPieces =
  do assertEqual 0 (numBlackPieces (makeBoard []))
     assertEqual 1 (numBlackPieces (read "__b_____'__wwww__'________"))
     assertEqual 3 (numBlackPieces (read "__w_____'b_www___'_b_wwb_w"))
     assertEqual 24 (numBlackPieces (read "bbbbbbbb'bbbbbbbb'bbbbbbbb"))
     assertEqual 0 (numBlackPieces (read "wwwwwwww'wwwwwwww'wwwwwwww"))

main :: IO ()
main = htfMain htf_thisModulesTests

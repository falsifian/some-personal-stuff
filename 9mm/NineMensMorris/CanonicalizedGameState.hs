module NineMensMorris.CanonicalizedGameState
( CanonicalizedState
, canonicalize
, start
, successorStates
) where

import qualified Data.Set as Set
import qualified NineMensMorris.GameState as GameState
import qualified NineMensMorris.GameOptions as GameOptions
import qualified NineMensMorris.Location as Location
import qualified NineMensMorris.SuccessorStates as SuccessorStates

newtype CanonicalizedState = Canonicalized GameState.GameState
                             deriving (Eq, Ord, Show)

canonicalize :: GameState.GameState -> CanonicalizedState
canonicalize state =
  let b = GameState.board state in
  Canonicalized
  ( GameState.buildGameState
    ( minimum
      [ mapLocations
        ( \(ring, fromTopLeft) -> ( transformRing ring
                                  , direction * fromTopLeft + offset
                                  )
        )
        b
      | transformRing <- [id, (\r -> 2 - r)]
      , direction <- [-1, 1]
      , offset <- [0, 2, 4, 6]
      ]
    )
    (GameState.numPiecesToPlace state)
  )

start :: CanonicalizedState
start = canonicalize GameState.start

successorStates :: GameOptions.Options -> CanonicalizedState -> [CanonicalizedState]
successorStates options (Canonicalized state) =
  Set.toList $ Set.fromList $
  map canonicalize (SuccessorStates.successorStates options state)

mapLocations :: ((Int, Int) -> (Int, Int)) -> GameState.Board ->
                GameState.Board
mapLocations f b = GameState.makeBoard
                   [ (Location.polar ring fromTopLeft, player)
                   | location <- Location.allLocations
                   , Just player <- return (GameState.pieceAt location b)
                   , let (ring, fromTopLeft) = f (Location.toPolar location)
                   ]

-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module NineMensMorris.MillTest where

import NineMensMorris.GameState (makeBoard, Player(..))
import NineMensMorris.Location (polar)
import NineMensMorris.Mill
import Test.Framework

assertFalse = assertBool . not

test_emptyNoMill :: IO ()
test_emptyNoMill = assertBool (not (inBlackMill (polar 1 1) (makeBoard [])))

test_simpleMills :: IO ()
test_simpleMills =
  do assertBool ( inBlackMill (polar 0 1)
                  (read "bbb_____'________'________")
                )
     assertBool ( inBlackMill (polar 1 2)
                  (read "________'__bbb___'________")
                )
     assertBool ( inBlackMill (polar 2 6)
                  (read "________'________'b_____bb")
                )
     assertBool ( inBlackMill (polar 2 5)
                  (read "_____b__'_____b__'_____b__")
                )

test_moreMillsAndNonMills :: IO ()
test_moreMillsAndNonMills =
  do assertBool ( inBlackMill (polar 1 2)
                  (read "________'bbbbb___'________")
                )
     assertBool ( inBlackMill (polar 1 1)
                  (read "bbbbbbbb'bbbbbbbb'bbbbbbbb")
                )
     assertFalse ( inBlackMill (polar 1 1)
                   (read "wwwwwwww'wwwwwwww'wwwwwwww")
                 )
     assertFalse ( inBlackMill (polar 0 1)
                   (read "__bbb___'________'________")
                 )
     assertBool ( inBlackMill (polar 2 7)
                  (read "www____b'_______b'____wwwb")
                )

main :: IO ()
main = htfMain htf_thisModulesTests

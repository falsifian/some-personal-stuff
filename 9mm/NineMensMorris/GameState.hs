module NineMensMorris.GameState
( Player(Black, White)
, Board
, DuplicateLocationException(..)
, GameState
, buildGameState
, board
, numPiecesToPlace
, start
, blackLocations
, change
, pieceAt
, makeBoard
, numBlackPieces
, switchPlayers
) where

import qualified Control.Exception as Exception
import Control.Monad (guard)
import qualified Data.Bits as Bits
import Data.Bits ((.&.))
import Data.Maybe (catMaybes)
import Data.Typeable (Typeable)
import Data.Word (Word64)
import qualified NineMensMorris.Location as Location

data Player = Black | White deriving (Eq, Ord, Show)

-- Format: bits 2*i and 2*i+1 encode the i-th location (bit 0 is least
-- significant bit). Location indexing is using Location.toInt.
-- 0 = empty, 2 = black, 3 = white.
newtype Board = Board Word64 deriving (Eq, Ord)

instance Read Board where
  readsPrec _prec r =
    let readSpace _x _y '_' = [Nothing]
        readSpace x y 'b' = [Just (Location.polar x y, Black)]
        readSpace x y 'w' = [Just (Location.polar x y, White)]
        readSpace _x _y _ = []
        readRing ring s =
          do guard (8 == length s)
             spaces <- sequence [readSpace ring i x | (i, x) <- zip [0..] s]
             return $ catMaybes spaces
    in do (token, s) <- lex r
          (ring0s, '\'' : rings12s) <- [splitAt 8 token]
          (ring1s, '\'' : ring2s) <- [splitAt 8 rings12s]
          ring0 <- readRing 0 ring0s
          ring1 <- readRing 1 ring1s
          ring2 <- readRing 2 ring2s
          return (makeBoard (ring0 ++ ring1 ++ ring2), s)

instance Show Board where
  show b =
    let showSpace Nothing = '_'
        showSpace (Just Black) = 'b'
        showSpace (Just White) = 'w'
        showRing r = [ showSpace (pieceAt (Location.polar r a) b)
                     | a <- [0..7]
                     ]
    in showRing 0 ++ '\'' : showRing 1 ++ '\'' : showRing 2

data DuplicateLocationException =
  DuplicateLocationException
  deriving (Eq, Show, Typeable)

instance Exception.Exception DuplicateLocationException

-- Format: board + 2^48 * number of pieces to place for both players.
-- Number of pieces to place starts at 18.
-- Black is always next to move.
newtype GameState = GameState Word64 deriving (Eq, Ord)

buildGameState :: Board -> Int -> GameState
buildGameState (Board b) n = GameState (Bits.shiftL (fromIntegral n) 48 + b)

board :: GameState -> Board
board (GameState s) = Board (s .&. (2^48 - 1))

numPiecesToPlace :: GameState -> Int
numPiecesToPlace (GameState s) = fromIntegral (Bits.shiftR s 48)

instance Show GameState where
  show s = show (board s) ++ "+" ++ show (numPiecesToPlace s)

start :: GameState
start = buildGameState (Board 0) 18

blackLocations :: Board -> [Location.Location]
blackLocations board =
  [ location
  | location <- Location.allLocations
  , pieceAt location board == Just Black
  ]

makeBoard :: [(Location.Location, Player)] -> Board
makeBoard =
  let update (l, p) b =
        case pieceAt l b of
          Nothing -> change l (Just p) b
          Just _ -> Exception.throw DuplicateLocationException
  in foldr update (Board 0)

pieceAt :: Location.Location -> Board -> Maybe Player
pieceAt location (Board board) =
  decodeSpace (mod (div board (locationBit location)) 4)

numBlackPieces :: Board -> Int
numBlackPieces (Board board) =
  Bits.popCount (0xaaaaaaaaaaaa .&. board .&. (Bits.complement (board * 2)))

switchPlayers :: Board -> Board
switchPlayers (Board b) =
  Board (Bits.xor (div (b .&. 0xaaaaaaaaaaaa) 2) b)

change :: Location.Location -> Maybe Player -> Board -> Board
change location new (Board board) =
  Board
  ( (board .&. Bits.complement (3 * locationBit location))
    + encodeSpace new * locationBit location
  )

decodeSpace :: Word64 -> Maybe Player
decodeSpace 0 = Nothing
decodeSpace 2 = Just Black
decodeSpace 3 = Just White

encodeSpace :: Maybe Player -> Word64
encodeSpace Nothing = 0
encodeSpace (Just Black) = 2
encodeSpace (Just White) = 3

locationBit :: Location.Location -> Word64
locationBit location =
  Bits.shiftL 1 (2 * Location.toInt location)

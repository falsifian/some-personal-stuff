module NineMensMorris.SuccessorStates
( successorStates
) where

import qualified Data.Map as Map
import Data.Maybe (isNothing)
import qualified NineMensMorris.GameOptions as GameOptions
import qualified NineMensMorris.GameState as GameState
import qualified NineMensMorris.Location as Location
import qualified NineMensMorris.Mill as Mill

-- Black has lost if moves s is empty.
successorStates :: GameOptions.Options -> GameState.GameState -> [GameState.GameState]
successorStates options state | GameState.numPiecesToPlace state == 0
  = let board = GameState.board state
        numBlackPieces = GameState.numBlackPieces board
    in if numBlackPieces < 3
       then []
       else if GameOptions.flying options && numBlackPieces == 3
            then flyingMoves board
            else slidingMoves board
successorStates _ state = placingMoves state

captures :: GameState.Board -> [GameState.Board]
captures board =
  let locationsNotInMill =
        [ blackLocation
        | blackLocation <- GameState.blackLocations board
        , not (Mill.inBlackMill blackLocation board)
        ]
      capturableLocations =
        if null locationsNotInMill
        then GameState.blackLocations board
        else locationsNotInMill
  in [ GameState.change location Nothing board
     | location <- capturableLocations
     ]

-- Assumes old /= new
movePiece :: GameState.Board -> Location.Location -> Location.Location ->
             [GameState.GameState]
movePiece board old new =
  let board' = GameState.change new (Just GameState.Black) $
               GameState.change old Nothing board
      board'' = GameState.switchPlayers board'
  in if Mill.inBlackMill new board'
     then [ GameState.buildGameState board''' 0
          | board''' <- captures board''
          ]
     else [GameState.buildGameState board'' 0]

placingMoves :: GameState.GameState -> [GameState.GameState]
placingMoves state =
  let board = GameState.board state
  in [ GameState.buildGameState board'' (GameState.numPiecesToPlace state - 1)
     | location <- Location.allLocations
     , isNothing (GameState.pieceAt location board)
     , let board' =
             GameState.switchPlayers
             (GameState.change location (Just GameState.Black) board)
     , board'' <-
       if Mill.inBlackMill location board
       then captures board'
       else [board']
     ]

flyingMoves :: GameState.Board -> [GameState.GameState]
flyingMoves board =
  let fromLocation blackLocation =
        concat
        [ movePiece board blackLocation newLocation
        | newLocation <- Location.allLocations
        , isNothing (GameState.pieceAt newLocation board)
        ]
  in concat (map fromLocation (GameState.blackLocations board))

slidingMoves :: GameState.Board -> [GameState.GameState]
slidingMoves board =
  let fromLocation blackLocation =
        concat
        [ movePiece board blackLocation newLocation
        | newLocation <- Location.adjacentLocations blackLocation
        , isNothing (GameState.pieceAt newLocation board)
        ]
  in concat (map fromLocation (GameState.blackLocations board))

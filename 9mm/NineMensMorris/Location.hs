module NineMensMorris.Location
( BadCoordinatesException(..)
, Location
, adjacentLocations
, allLocations
, polar
, toPolar
, toInt
, fromInt
) where

import qualified Control.Exception as Exception
import Data.Typeable (Typeable)
import Prelude hiding (cycle)

cycle :: Int -> Int -> Int
cycle x y = mod (x + y) 8

-- Represented as an int in the range 0..23.
newtype Location = Location {toInt :: Int} deriving (Eq, Ord, Show)

adjacentLocations :: Location -> [Location]
adjacentLocations location =
  let (r, fromTopLeft) = toPolar location in
  [polar r (cycle fromTopLeft 1), polar r (cycle fromTopLeft (-1))]
  ++ if (mod fromTopLeft 2 == 1)
     then [ polar r' fromTopLeft
          | r' <- [r - 1, r + 1]
          , r' >= 0 && r' <= 2
          ]
     else []

allLocations :: [Location]
allLocations = map Location [0..23]

-- Input: ring, location in ring (0 = top-left, 1 = top, etc, clockwise).
-- Location in ring wraps around and can be any integer.
polar :: Int -> Int -> Location
polar ring fromTopLeft
  | ring < 0 || ring >= 3 =
    Exception.throw BadCoordinatesException
  | otherwise = Location (ring * 8 + (mod fromTopLeft 8))

toPolar :: (Num a, Num b) => Location -> (a, b)
toPolar (Location l) = (fromIntegral (div l 8), fromIntegral (mod l 8))

-- Encoding as an int in the range 0..23
fromInt :: Int -> Location
fromInt = Location

data BadCoordinatesException =
  BadCoordinatesException
  deriving (Eq, Show, Typeable)

instance Exception.Exception BadCoordinatesException

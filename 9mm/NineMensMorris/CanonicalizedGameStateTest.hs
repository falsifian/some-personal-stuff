-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module NineMensMorris.CanonicalizedGameStateTest where

import qualified Data.List as List
import NineMensMorris.CanonicalizedGameState
import qualified NineMensMorris.GameOptions as GameOptions
import qualified NineMensMorris.GameState as GameState
import NineMensMorris.GameState (buildGameState)
import Test.Framework

test_startIsCanonicalizedStart :: IO ()
test_startIsCanonicalizedStart = assertEqual (canonicalize GameState.start) start

test_canonicalizeMakesSomePositionsEqual :: IO ()
test_canonicalizeMakesSomePositionsEqual =
  do assertEqual
       ( canonicalize ( buildGameState
                        (read "b_______'________'________")
                        17
                      )
       )
       ( canonicalize ( buildGameState
                        (read "__b_____'________'________")
                        17
                      )
       )
     assertEqual
       ( canonicalize ( buildGameState
                        (read "_w_w_w__'bwbwb__w'b_b_b___")
                        0
                      )
       )
       ( canonicalize ( buildGameState
                        (read "b_b___b_'bwb__wbw'_w_w___w")
                        0
                      )
       )
     assertEqual
       ( canonicalize ( buildGameState
                        (read "w_______'_b______'__b_____")
                        16
                      )
       )
       ( canonicalize ( buildGameState
                        (read "__w_____'_b______'b_______")
                        16
                      )
       )

test_canonicalizeKeepsSomePositionsUnequal :: IO ()
test_canonicalizeKeepsSomePositionsUnequal =
  do assertNotEqual
       ( canonicalize ( buildGameState
                        (read "b_______'________'________")
                        17
                      )
       )
       ( canonicalize ( buildGameState
                        (read "_b______'________'________")
                        17
                      )
       )
     assertNotEqual
       ( canonicalize ( buildGameState
                        (read "b_______'________'_w______")
                        16
                      )
       )
       ( canonicalize ( buildGameState
                        (read "__b_____'________'_______w")
                        16
                      )
       )
     assertNotEqual
       ( canonicalize ( buildGameState
                        (read "bbb_____'________'www_____")
                        12
                      )
       )
       ( canonicalize ( buildGameState
                        (read "bbb_____'________'www_____")
                        10
                      )
       )

test_startHasFourSuccessors :: IO ()
test_startHasFourSuccessors =
  assertEqual
  4
  ( length
    ( successorStates
      (GameOptions.Options {GameOptions.flying = False})
      start
    )
  )

test_successorStates :: IO ()
test_successorStates =
  do assertElem
       ( canonicalize
         ( GameState.buildGameState
           (read "bw______'________'________")
           16
         )
       )
       ( successorStates
         (GameOptions.Options {GameOptions.flying = False})
         ( canonicalize
           ( GameState.buildGameState
             ( read "w_______'________'________")
             17
           )
         )
       )
     assertBool
       ( not $
         List.elem
         ( canonicalize
           ( GameState.buildGameState
             (read "_w______'b_______'________")
             16
           )
         )
         ( successorStates
           (GameOptions.Options {GameOptions.flying = False})
           ( canonicalize
             ( GameState.buildGameState
               (read "b_______'________'________")
               17
             )
           )
         )
       )

test_successorStatesObeysFlying :: IO ()
test_successorStatesObeysFlying =
  let before = canonicalize $
               GameState.buildGameState
               (read "bbb_____'wwwwwwww'________")
               0
      after = canonicalize $
              GameState.buildGameState
              (read "ww______'bbbbbbbb'w_______")
              0
  in do assertElem
          after
          ( successorStates
            (GameOptions.Options {GameOptions.flying = True})
            before
          )
        assertBool
          ( not $ List.elem
            after
            ( successorStates
              (GameOptions.Options {GameOptions.flying = False})
              before
            )
          )

main :: IO ()
main = htfMain htf_thisModulesTests

{-# LANGUAGE ScopedTypeVariables #-}

module StepGame where

import Data.Array
import Numeric.LinearProgramming ((#), Bound(..))
import qualified Numeric.LinearProgramming as LP

-- Player 0 minimizes and player 1 maximizes.
zsgValue :: (Ix player0Move, Ix player1Move) =>
            Array (player0Move, player1Move) Double -> Double
zsgValue values =
  let ((p0MinMove, p1MinMove), (p0MaxMove, p1MaxMove)) = bounds values
      p0MoveBounds = (p0MinMove, p0MaxMove)
      numP0Moves = rangeSize p0MoveBounds
      allP0Moves = range p0MoveBounds
      allP1Moves = range (p1MinMove, p1MaxMove)
      LP.Optimal (solutionValue, _) = LP.exact
                   (LP.Minimize (replicate numP0Moves 0 ++ [1]))
                   (LP.Sparse
                    ([
                        ([ (values ! (p0Move, p1Move)) # (index p0MoveBounds p0Move + 1)
                         | p0Move <- allP0Moves
                         ] ++
                         [(-1) # (numP0Moves + 1)]
                        ) :<=: 0
                     | p1Move <- allP1Moves
                     ]
                     ++
                     [ [ 1 # (index p0MoveBounds p0Move + 1) | p0Move <- allP0Moves ] :==: 1 ]
                    )
                   )
                   ([(v + 1) :&: (0, 1) | v <- [0 .. numP0Moves - 1]]
                    ++ [Free (numP0Moves + 1)])
  in solutionValue

solveAll :: forall balance position.
            (Ix balance, Num balance, Ix position, Num position) =>
            position -> balance -> balance ->
            Array (position, balance, balance) Double
solveAll boardSize balance0Limit balance1Limit =
  let solutionBounds = ((-1, 0, 0), (boardSize, balance0Limit, balance1Limit))
      solution :: Array (position, balance, balance) Double
      solution = listArray solutionBounds (map solve (range solutionBounds))
      solve :: (position, balance, balance) -> Double
      solve (-1, _, _) = -1
      solve (p, _, _) | boardSize == p = 1
      solve (0, b0, b1) | b0 > b1 = -1
      solve (p, b0, b1) | boardSize - 1 == p && b0 < b1 = 1
      solve (_, 0, 0) = 0
      solve (p, b0, 0) = solution ! (p - 1, b0 - 1, 0)
      solve (p, 0, b1) = solution ! (p + 1, 0, b1 - 1)
      solve (position, balance0, balance1) =
        let moveBounds = ((1, 1), (balance0, balance1))
        in zsgValue
           (listArray moveBounds
            [ solution ! (position', balance0 - spend0, balance1 - spend1)
            | (spend0, spend1) <- range moveBounds
            , let position' =
                    case compare spend0 spend1 of
                      LT -> position + 1
                      EQ -> position
                      GT -> position - 1
            ]
           )
  in solution

{-# LANGUAGE FlexibleContexts, RankNTypes #-}

module Evaluation (trainAndComputeAccuracy, trainAndComputeAccuracyWithHeldOut) where

import qualified Data.Array.Repa as Repa
import Data.Array.Repa.Index ((:.)((:.)))
import Dataset


-- Interface

type Trainer m a = Monad m => forall r. Dataset r -> m a

-- Given images with shape [# images, 28, 28], return labels in the range 0..9.
type Predictor m a = Monad m => a -> forall r. Repa.Source r Double => Repa.Array r Repa.DIM3 Double -> m (Repa.Array Repa.U Repa.DIM1 Int)

trainAndComputeAccuracy :: Monad m => Dataset r0 -> Trainer m a -> Predictor m a -> Dataset r1 -> m Double

-- The dataset should already be shuffled.
trainAndComputeAccuracyWithHeldOut :: Monad m => Double -> Dataset r -> Trainer m a -> Predictor m a -> m Double


-- Implementation

extractSubDataset :: Int -> Int -> Dataset r -> Dataset Repa.D
extractSubDataset startExample numExamples dataset@(Dataset {}) =
  let Repa.Z :. _ :. numRows :. numColumns = Repa.extent (features dataset) in
  Dataset
  { features = Repa.extract
               (Repa.Z :. startExample :. 0 :. 0)
               (Repa.Z :. numExamples :. numRows :. numColumns)
               (features dataset)
  , labels = Repa.extract
             (Repa.Z :. 0)
             (Repa.Z :. numExamples)
             (labels dataset)
  }

trainAndComputeAccuracy trainingExamples train predict evaluationExamples@(Dataset {}) =
    let Repa.Z :. numEvaluationExamples :. _ :. _ = Repa.extent (features evaluationExamples) in
    do model <- train trainingExamples
       predictions <- predict model (features evaluationExamples)
       let True = Repa.extent (labels evaluationExamples) == Repa.extent predictions  -- TODO: make sure this actually asserts
       numCorrect <- Repa.sumAllP $ Repa.zipWith (\x y -> if x == y then 1 :: Int else 0) (labels evaluationExamples) predictions
       return $ fromIntegral numCorrect / fromIntegral numEvaluationExamples

trainAndComputeAccuracyWithHeldOut heldOutFraction dataset@(Dataset {}) train predict =
  let Repa.Z :. numExamples :. numRows :. numColumns = Repa.extent (features dataset)
      numTrainingExamples = round (fromIntegral numExamples * heldOutFraction)
      trainingExamples = extractSubDataset 0 numTrainingExamples dataset
      evaluationExamples = extractSubDataset numTrainingExamples
                           (numExamples - numTrainingExamples) dataset
  in trainAndComputeAccuracy trainingExamples train predict evaluationExamples

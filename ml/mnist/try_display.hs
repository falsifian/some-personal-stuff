module Main where

import Display
import Input

main :: IO ()
main = do trainingSet <- readTraining
          putStr (showDigit trainingSet 22)

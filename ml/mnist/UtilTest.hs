-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module UtilTest where

import qualified Data.Array.Repa as Repa
import           Data.Array.Repa.Index ((:.)((:.)))
import           Data.Word (Word8)
import           Test.Framework
import           Util


-- Test argmax

test_argmaxFindsArgmax :: IO ()
test_argmaxFindsArgmax =
  do out0 <- Repa.computeP $ argmax (Repa.fromListUnboxed (Repa.Z :. (4::Int)) [3, -2, 8, 0 :: Int])
     assertEqual (Repa.fromListUnboxed Repa.Z [2]) out0
     out1 <- Repa.computeP $ argmax ( Repa.fromListUnboxed (Repa.Z :. (3::Int) :. (1::Int) :. (2::Int))
                                      [0, 1.5, 1.5, 0, -5, -4 :: Double]
                                    )
     assertEqual (Repa.fromListUnboxed (Repa.Z :. 3 :. 1) [1, 0, 1]) out1

-- Test constArray and zeros.

test_constArrayHasCorrectShape :: IO ()
test_constArrayHasCorrectShape =
  do assertEqual (Repa.Z :. 1) $ Repa.extent $ constArray (Repa.Z :. (1::Int)) undefined
     assertEqual Repa.Z $ Repa.extent $ constArray Repa.Z 0
     assertEqual (Repa.Z :. 5 :. 1 :. 2) $ Repa.extent $
       constArray (Repa.Z :. (5::Int) :. (1::Int) :. (2::Int)) 42.2
     assertEqual (Repa.Z :. 0 :. 1 :. 2 :. 0) $ Repa.extent $
       constArray (Repa.Z :. (0::Int) :. (1::Int) :. (2::Int) :. (0::Int)) "hi"

test_constArrayHasCorrectValue :: IO ()
test_constArrayHasCorrectValue =
  do assertEqual 2 $ Repa.index (constArray (Repa.Z :. (1::Int)) 2) (Repa.Z :. 0)
     assertEqual "test" $ Repa.index (constArray (Repa.Z :. (1::Int) :. (2::Int) :. (3::Int)) "test") (Repa.Z :. 0 :. 1 :. 1)

test_zerosHasCorrectShape :: IO ()
test_zerosHasCorrectShape =
  do assertEqual Repa.Z $ Repa.extent $ constArray Repa.Z 0
     assertEqual (Repa.Z :. 5 :. 1) $ Repa.extent $
       zeros (Repa.Z :. (5::Int) :. (1::Int))

test_zerosHasCorrectValue :: IO ()
test_zerosHasCorrectValue =
  do assertEqual 0 $ Repa.index (zeros (Repa.Z :. (3::Int))) (Repa.Z :. 1)


-- Test counts.

test_countsCountsEmptyArray :: IO ()
test_countsCountsEmptyArray =
  do c <- counts 3 (Repa.fromListUnboxed (Repa.Z :. (0::Int)) [])
     assertEqual [0, 0, 0 :: Int] $ Repa.toList c

test_countsCountsValues :: IO ()
test_countsCountsValues =
  do c0 <- counts 5 (Repa.fromListUnboxed (Repa.Z :. (6::Int)) [0, 1, 3, 1, 1, 1])
     assertEqual [1, 4, 0, 1, 0 :: Int] $ Repa.toList c0
     -- Try another example and try Word8 instead of Int.
     c1 <- counts 3 (Repa.fromListUnboxed (Repa.Z :. (2::Int)) [2, 0])
     assertEqual [1, 0, 1 :: Word8] $ Repa.toList c1

test_countsIgnoresOutofRangeValues :: IO ()
test_countsIgnoresOutofRangeValues =
  do c <- counts 1 (Repa.fromListUnboxed (Repa.Z :. (6::Int)) [-1, -1, 0, 0, 0, 1])
     assertEqual [3 :: Int] $ Repa.toList c


-- Test toOneHot.

test_toOneHotEncodesSingleNumber :: IO ()
test_toOneHotEncodesSingleNumber =
  do out0 <- Repa.computeP $ toOneHot 4 (constArray Repa.Z 2)
     assertEqual (Repa.fromListUnboxed (Repa.Z :. 4) [0, 0, 1, 0::Double]) out0
     out1 <- Repa.computeP $ toOneHot 4 (constArray Repa.Z 0)
     assertEqual (Repa.fromListUnboxed (Repa.Z :. 4) [1, 0, 0, 0::Int]) out1

test_toOneHotEncodesDim1Array :: IO ()
test_toOneHotEncodesDim1Array =
  do out <- Repa.computeP $ toOneHot 3 (Repa.fromListUnboxed (Repa.Z :. (4::Int)) [0, 2, 1, 2])
     assertEqual (Repa.fromListUnboxed (Repa.Z :. 4 :. 3) [1::Int, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1]) out

test_toOneHotEncodesMultiDimArray :: IO ()
test_toOneHotEncodesMultiDimArray =
  do out <- Repa.computeP $ toOneHot 2 (Repa.fromListUnboxed (Repa.Z :. (1::Int) :. (2::Int) :. (1::Int) :. (3::Int)) [1, 0, 0, 0, 1, 0])
     assertEqual (Repa.fromListUnboxed (Repa.Z :. 1 :. 2 :. 1 :. 3 :. 2) [0::Double, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0]) out

main :: IO ()
main = htfMain htf_thisModulesTests

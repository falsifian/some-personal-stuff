module Main where

import Evaluation
import Input
import qualified Models.LogisticRegression as LR
import Shuffle (shuffleDataset)
import System.Random (newStdGen)
import Text.Printf (printf)
import TimeLog

parameters :: LR.Hyperparams
parameters = LR.Hyperparams
             { LR.optimizationMode = LR.NaiveGradientDescent
                                     {LR.learningRate = 1e-4, LR.numSteps = 10}
             }

main :: IO ()
main = do trainingSet <- readTraining
          randomGen <- newStdGen
          timeLog "Shuffling..."
          trainingSet' <- shuffleDataset randomGen trainingSet
          timeLog "Done shuffling."
          accuracy <- trainAndComputeAccuracyWithHeldOut 0.2 trainingSet (LR.train parameters) LR.predict
          printf "Accuracy: %f\n" accuracy


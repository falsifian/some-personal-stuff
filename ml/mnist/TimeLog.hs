module TimeLog (timeLog) where

import Data.Time.Clock (getCurrentTime)
import System.IO (hFlush, stdout)

timeLog :: String -> IO ()
timeLog message = do now <- getCurrentTime
                     putStrLn (show now ++ ": " ++ message)
                     -- Flush stdout, just in case. It doesn't seem to help: I
                     -- still sometimes see a line delayed.
                     hFlush stdout

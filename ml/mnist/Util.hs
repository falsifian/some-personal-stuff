{-# LANGUAGE FlexibleContexts, TypeFamilies, TypeOperators #-}

module Util where

import           Control.Exception (assert)
import qualified Data.Array.Repa as Repa
import           Data.Array.Repa.Index ((:.)((:.)))
import           Data.Array.Repa.Repr.Unboxed as RepaU

argmax :: (Repa.Shape sh, Repa.Source r a, Ord a) => Repa.Array r (sh :. Int) a -> Repa.Array Repa.D sh Int
argmax x =
  let _ :. lastDimSize = Repa.extent x
      computeOneArgmax lookup i =
        let f bestIndex bestValue j | j >= lastDimSize = bestIndex
                                    | otherwise        = let thisValue = lookup (i :. j) in
                                                         if thisValue > bestValue
                                                         then f j thisValue (j+1)
                                                         else f bestIndex bestValue (j+1)
        in f 0 (lookup (i :. 0)) 1
  in assert (lastDimSize > 0) $ Repa.traverse x (\(x :. _) -> x) computeOneArgmax

-- TODO: Is it faster to build a 1x1x... and use Repa.extend?
constArray :: sh -> a -> Repa.Array Repa.D sh a
constArray shape value = Repa.fromFunction shape (const value)

-- counts 5 [0, 1, 3, 1, 1, 1] returns [1, 4, 0, 1, 0].
counts :: (Repa.Source r Int, Num a, RepaU.Unbox a, Monad m) => Int -> Repa.Array r Repa.DIM1 Int -> m (Repa.Array Repa.U Repa.DIM1 a)
counts valueRange values =
  Repa.sumP $ Repa.transpose $ toOneHot valueRange values

-- toOneHot 4 [0, 0, 2] is [[1,0,0,0],[1,0,0,0],[0,1,0,0]].
toOneHot :: (Num a, Repa.Source r Int, Repa.Shape sh, Repa.Slice sh,
             -- These two equality constraints should be satisfied by any
             -- reasonable shape. They're guaranteed to be satisfied if sh is
             -- constructed using Repa.Z and (Repa.:.).
             Repa.SliceShape sh ~ Repa.Z, Repa.FullShape sh ~ sh) =>
            Int -> Repa.Array r sh Int -> Repa.Array Repa.D (sh :. Int) a
toOneHot valueRange values =
  let inputShape = Repa.extent values
      repeatedValues = Repa.extend (Repa.Any :. (valueRange::Int)) values
      targetValues = Repa.extend (inputShape :. Repa.All) (Repa.fromListUnboxed (Repa.Z :. valueRange) [0..valueRange-1])
  in  Repa.zipWith (\x y -> if x == y then 1 else 0) repeatedValues targetValues

zeros :: Num a => sh -> Repa.Array Repa.D sh a
zeros shape = constArray shape 0

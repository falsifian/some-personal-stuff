{-# LANGUAGE FlexibleContexts, NamedFieldPuns #-}

-- A kind of model that always predicts the same label.

module Models.Constant where

import qualified Data.Array.Repa as Repa
import Data.Array.Repa.Index ((:.)((:.)))
import Dataset
import Util (counts)


-- Interface

data ConstantModel = AlwaysPredict !Int

train :: Monad m => Dataset r -> m ConstantModel

predict :: (Repa.Source r Double, Monad m) => ConstantModel -> Repa.Array r Repa.DIM3 Double -> m (Repa.Array Repa.U Repa.DIM1 Int)


-- Implementation

train (Dataset {labels}) =
  do labelCounts <- counts 10 labels
     let (_, argmax) = maximum (zip (Repa.toList labelCounts :: [Int]) [0..9 :: Int])
     return (AlwaysPredict argmax)

predict (AlwaysPredict x) features =
  let Repa.Z :. numExamples :. _ :. _ = Repa.extent features in
  Repa.computeP $ Repa.fromFunction (Repa.Z :. numExamples) (const x)

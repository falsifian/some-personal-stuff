{-# LANGUAGE FlexibleContexts, NamedFieldPuns #-}

module Models.LogisticRegression (OptimizationMode(..), Hyperparams(..), Model, train, predict) where

import qualified Data.Array.Repa as Repa
import           Data.Array.Repa ((-^), (*^), (/^))
import           Data.Array.Repa.Algorithms.Matrix (mmultP, transpose2P)
import           Data.Array.Repa.Index ((:.)((:.)))
import qualified Data.Time.Clock as Clock
import           Dataset
import           TimeLog
import           Text.Printf (printf)
import           Util

-- Interface

data OptimizationMode =
  -- Gradient descent with a fixed learning rate and number of steps.
  NaiveGradientDescent {learningRate :: Double, numSteps :: Int}

data Hyperparams = Hyperparams {optimizationMode :: OptimizationMode}

data Model = Model
             { -- For each class, flatten the 28x28 coefficients into a 1D
               -- array, and prepend a bias term. This value is a (# classes) x
               -- (# features) array.
               coefficients :: Repa.Array Repa.U Repa.DIM2 Double
             }

-- We use the IO monad so we can print log messages.
train :: Hyperparams -> Dataset r -> IO Model

predict :: (Repa.Source r Double, Monad m) => Model -> Repa.Array r Repa.DIM3 Double -> m (Repa.Array Repa.U Repa.DIM1 Int)

-- Implementation

logInterval :: Clock.NominalDiffTime
logInterval =
  let oneSecond = Clock.nominalDay / (24 * 60 * 60)
  in 60 * oneSecond

-- TODO: Find places to use mmultP. See if this becomes faster when I switch to it.
train (Hyperparams {optimizationMode = NaiveGradientDescent {learningRate, numSteps}}) (Dataset {features, labels}) =
  do timeLog "Flattening features"
     convertedFeatures <- convertFeatures features
     let Repa.Z :. numExamples :. numFeatures = Repa.extent convertedFeatures

     timeLog "Pre-computing stuff."

     let oneHotLabels = toOneHot 10 labels  -- numExamples x (# classes)

     -- (# classes) x numFeatures
     featureSumsByClass <- Repa.sumP $
          Repa.extend (Repa.Z :. (10::Int) :. Repa.All :. Repa.All) (Repa.transpose convertedFeatures)
       *^ Repa.extend (Repa.Z :. Repa.All :. numFeatures :. Repa.All) (Repa.transpose oneHotLabels)
       :: IO (Repa.Array Repa.U Repa.DIM2 Double)

     let -- Returns deferred computations of the logistic loss and the gradient
         -- of that loss with respect to the coefficients in the model.
         computeGradientAndLoss :: Model -> IO (IO Double, IO (Repa.Array Repa.D Repa.DIM2 Double))
         computeGradientAndLoss (Model {coefficients}) =
           do timeLog "Computing logits."
              -- logits is numExamples x (# classes)
              logits <- Repa.sumP $
                -- numExamples x (# classes) x numFeatures
                   Repa.extend (Repa.Z :. numExamples :. Repa.All :. Repa.All) coefficients 
                *^ Repa.extend (Repa.Z :. Repa.All :. (10::Int) :. Repa.All) convertedFeatures
              timeLog "Computing expLogits."
              expLogits <- Repa.computeUnboxedP $ Repa.map exp logits
              timeLog "Computing denominator."
              -- The denominator of the partition function. Also the
              -- denominator of the gradient of the log of the partition
              -- function.
              -- 1-D array of size numExamples
              denominator <- Repa.sumP expLogits
              timeLog "Done computing denominator."

              let computeGradient =
                   do -- The numerator of the gradient of the partition function. Shape
                      -- (# classes) x numFeatures x numExamples
                      let partitionGradientNumerator =
                              Repa.extend (Repa.Z :. Repa.All :. numFeatures :. Repa.All) (Repa.transpose oneHotLabels)
                           *^ Repa.extend (Repa.Z :. (10::Int) :. Repa.All :. Repa.All) (Repa.transpose convertedFeatures)
                           *^ Repa.extend (Repa.Z :. Repa.All :. numFeatures :. Repa.All) (Repa.transpose expLogits)
                      timeLog "Computing partition Gradient."
                      partitionGradient <- Repa.sumP $
                           partitionGradientNumerator
                        /^ Repa.extend (Repa.Z :. (10::Int) :. numFeatures :. Repa.All)
                                       denominator
                      timeLog "Done computing partition Gradient."
                      return ( Repa.zipWith (\x y -> (-x + y) / fromIntegral numExamples)
                                            featureSumsByClass partitionGradient
                             :: Repa.Array Repa.D Repa.DIM2 Double)

              let computeLoss = do timeLog "Computing sumLogNumerator."
                                   sumLogNumerator <- Repa.sumAllP $
                                     -- numExamples x (# classes) x numFeatures
                                        Repa.extend (Repa.Z :. Repa.All :. Repa.All :. numFeatures) oneHotLabels
                                     *^ Repa.extend (Repa.Z :. numExamples :. Repa.All :. Repa.All) coefficients
                                     *^ Repa.extend (Repa.Z :. Repa.All :. (10::Int) :. Repa.All) convertedFeatures
                                   timeLog "Computing sumLogPartition."
                                   sumLogPartition <- Repa.sumAllP $ Repa.map log denominator
                                   timeLog "Effectively done computing loss."
                                   return $ (-sumLogNumerator + sumLogPartition) / fromIntegral numExamples
                                        
              return (computeLoss, computeGradient)

     timeLog (printf "Starting naïve gradient descent. learning rate %f, # steps %d." learningRate numSteps)

     let continueGradientDescent :: Int -> Model -> Clock.UTCTime -> IO Model
         continueGradientDescent stepsTaken model@(Model {coefficients}) nextLogTime =
           do (computeLoss, computeGradient) <- computeGradientAndLoss model
              now <- Clock.getCurrentTime
              nextLogTime' <- if now >= nextLogTime || stepsTaken >= numSteps
                then do loss <- computeLoss
                        timeLog $ printf "Avg loss after %d steps: %f" stepsTaken loss
                        return $ Clock.addUTCTime logInterval now
                else return nextLogTime
              if stepsTaken < numSteps
                then do gradient <- computeGradient
                        timeLog "Computing updated coefficients."
                        coefficients' <- Repa.computeUnboxedP $ coefficients -^ Repa.map (learningRate *) gradient
                        timeLog "Done computing updated coefficients."
                        continueGradientDescent (1 + stepsTaken) (Model {coefficients = coefficients'}) nextLogTime'
                else return model

     timeLog "Computing initial coefficients."
     initialCoefficients <- Repa.computeUnboxedP $ zeros (Repa.Z :. (10::Int) :. numFeatures)
     timeLog "Done computing initial coefficients."
     now <- Clock.getCurrentTime
     continueGradientDescent 0 (Model {coefficients = initialCoefficients}) now

predict (Model {coefficients}) features =
  do convertedFeatures <- convertFeatures features
     let Repa.Z :. numExamples :. numFeatures = Repa.extent convertedFeatures
     -- TODO: This is silly. mmultP is just going to transpose it again.
     transposedFeatures <- transpose2P convertedFeatures
     logits <- mmultP coefficients transposedFeatures
     Repa.computeP $ argmax logits

-- Flattens the 28x28 features into one dimension, and prepends a constant 1
-- feature to each example to go with the bias coefficient. Returns a
-- (# examples) x (28*28+1) array.
convertFeatures :: (Repa.Source r Double, Monad m) => Repa.Array r Repa.DIM3 Double -> m (Repa.Array Repa.U Repa.DIM2 Double)
convertFeatures features =
  let Repa.Z :. numExamples :. numRows :. numCols = Repa.extent features
      numFeaturesNoBias = numRows * numCols
      flatFeaturesNoBias = Repa.reshape (Repa.Z :. numExamples :. numFeaturesNoBias) features
      biasFeature = constArray (Repa.Z :. numExamples :. 1) 1
  in Repa.computeUnboxedP $ Repa.append biasFeature flatFeaturesNoBias

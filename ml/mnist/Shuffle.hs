module Shuffle (shuffleDataset) where

import qualified Control.DeepSeq as DeepSeq
import qualified Data.Array.Repa as Repa
import qualified Data.Array.Repa.Eval as RepaEval
import           Data.Array.Repa.Index ((:.)((:.)))
import           Dataset
import           System.Random (RandomGen)
import           System.Random.Shuffle (shuffle')
import           TimeLog


-- Interface

-- IO so we can log
-- This consumes the RandomGen value. The caller should probably use newStdGen or System.Random.split to generate it.
shuffleDataset :: (RandomGen g) => g -> Dataset r -> IO (Dataset Repa.U)


-- Implementation

shuffleDataset randomGen dataset@(Dataset {}) =
  let (featuresShape@(Repa.Z :. numExamples :. numRows :. numColumns),
       featuresFn) = Repa.toFunction (features dataset)
      labelsShape = Repa.extent (labels dataset)
      (_, labelsFn) = Repa.toFunction (labels dataset)
      shuffleOrder = shuffle' [0..numExamples-1] numExamples randomGen
  in do timeLog "Generating permutation."
        shuffleOrderArray <- RepaEval.now $ Repa.fromListUnboxed (Repa.Z :. numExamples) shuffleOrder
        timeLog "Shuffling features."
        let (_, shuffleOrderFn) = Repa.toFunction shuffleOrderArray
        let shuffledFeaturesFn :: Repa.DIM3 -> Double
            shuffledFeaturesFn (Repa.Z :. i :. r :. c) = featuresFn (Repa.Z :. shuffleOrderFn (Repa.Z :. i) :. r :. c)
        shuffledFeatures <- Repa.computeUnboxedP $ Repa.fromFunction featuresShape shuffledFeaturesFn
        timeLog "Shuffling labels."
        shuffledLabels <- RepaEval.now $ Repa.fromListUnboxed labelsShape [(Repa.!) (labels dataset) (Repa.Z :. i) | i <- shuffleOrder]
        timeLog "Done shuffling."
        return $ Dataset {features = shuffledFeatures, labels = shuffledLabels}

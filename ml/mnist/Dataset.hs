{-# LANGUAGE FlexibleContexts, GADTs #-}

module Dataset (Dataset(Dataset, features, labels)) where

import qualified Data.Array.Repa as Repa

data Dataset r where
  Dataset :: (Repa.Source r Double, Repa.Source r Int) => { -- Shape is [# images, 28, 28]. Elements are in the range [0, 1], each
    -- computed by dividing a Word8 from the original MNIST input by 255.
    features :: Repa.Array r Repa.DIM3 Double
    -- Every element is a label in the range 0..9.
  , labels :: Repa.Array r Repa.DIM1 Int
  } -> Dataset r

{-# LANGUAGE NamedFieldPuns #-}

module Display (showDigit) where

import qualified Data.Array.Repa as Repa
import Data.Array.Repa.Index ((:.)((:.)))
import Dataset
import Input

-- Interface

showDigit :: Dataset r -> Int -> String

-- Implementation

showDigit (Dataset {features}) i =
    let (Repa.Z :. _ :. numRows :. numCols, lookup) = Repa.toFunction features
        showRow r = [pixelToChar (lookup (Repa.Z :. i :. r :. c)) | c <- [0..numCols-1]]
    in unlines [showRow r | r <- [0..numRows-1]]

pixelToChar :: Double -> Char
pixelToChar p | p * 16 < 1 = ' '
              | p * 16 < 2 = '.'
              | p * 16 < 4 = '-'
              | p * 16 < 8 = '='
              | True = '#'

module Main where

import Evaluation
import Input
import qualified Models.Constant as C
import Shuffle (shuffleDataset)
import System.Random (newStdGen)
import Text.Printf (printf)
import TimeLog

main :: IO ()
main = do trainingSet <- readTraining
          randomGen <- newStdGen
          timeLog "Shuffling..."
          trainingSet' <- shuffleDataset randomGen trainingSet
          timeLog "Done shuffling."
          accuracy <- trainAndComputeAccuracyWithHeldOut 0.2 trainingSet C.train C.predict
          printf "Accuracy: %f\n" accuracy

module Input (readTraining, readTest) where

import Control.Exception (assert)
import qualified Data.Array.Repa as Repa
import qualified Data.Array.Repa.Repr.ByteString as RepaByteString
import qualified Data.Array.Repa.Eval as RepaEval
import Data.Array.Repa.Index ((:.)((:.)))
import qualified Data.ByteString as ByteString
import Data.ByteString (ByteString)
import Data.Int (Int32)
import Data.Word (Word8)
import Dataset
import System.FilePath.Posix (FilePath, joinPath)
import Text.Printf
import TimeLog

-- Interface

readTraining :: IO (Dataset Repa.U)
readTest :: IO (Dataset Repa.U)

-- Implementation

baseDir :: FilePath
-- Compressed dataset stored at /home/james/big/data-sets/mnist on angel
-- http://yann.lecun.com/exdb/mnist/
baseDir = "/home/james/tmp/mnist"

readFilesWithPrefix :: String -> IO (Dataset Repa.U)
readFilesWithPrefix prefix =
  let featuresPath = joinPath [baseDir, prefix ++ "-images-idx3-ubyte"]
      labelsPath = joinPath [baseDir, prefix ++ "-labels-idx1-ubyte"]
  in do timeLog (printf "Reading features from %s." featuresPath)
        featureData <- ByteString.readFile featuresPath
        timeLog "Parsing features."
        featureWords <- RepaEval.now $ parseIdx 2051 3 buildDim3 featureData
        timeLog "Transforming features."
        features <- Repa.computeP $ Repa.map ((/ 255) . fromIntegral :: Word8 -> Double) $ featureWords
        timeLog (printf "Reading labels from %s." labelsPath)
        labelData <- ByteString.readFile labelsPath
        timeLog "Parsing labels."
        labelWords <- RepaEval.now $ parseIdx 2049 1 buildDim1 labelData
        timeLog "Transforming labels."
        labels <- Repa.computeP $ Repa.map fromIntegral $ labelWords
        timeLog (printf "Done loading files with prefix %s." (show prefix))
        return (Dataset {features = features, labels = labels})

buildDim1 :: Integral a => [a] -> Repa.DIM1
buildDim1 l = case map fromIntegral l :: [Int] of [x] -> Repa.Z :. x

buildDim3 :: Integral a => [a] -> Repa.DIM3
buildDim3 l = case map fromIntegral l :: [Int] of [x, y, z] -> Repa.Z :. x :. y :. z

get32 :: ByteString -> (Int32, ByteString)
get32 s = let (h, t) = ByteString.splitAt 4 s
          in (foldl (\a x -> a * 256 + fromIntegral x) 0 (ByteString.unpack h), t)

get32s' :: [Int32] -> Int -> ByteString -> ([Int32], ByteString)
get32s' acc n s | n == 0 = (reverse acc, s)
                | True   = let (h, t) = get32 s in get32s' (h : acc) (n-1) t

get32s :: Int -> ByteString -> ([Int32], ByteString)
get32s = get32s' []

parseIdx :: Repa.Shape sh => Int -> Int -> ([Int32] -> sh) -> ByteString -> Repa.Array RepaByteString.B sh Word8
parseIdx magic numDimension buildShape idxData =
  let (magic', d) = get32 idxData
      (shape, d') = get32s numDimension d
  in assert (fromIntegral (product shape) == ByteString.length d') $
     RepaByteString.fromByteString (buildShape shape) d'

readTraining = readFilesWithPrefix "train"
readTest = readFilesWithPrefix "t10k"

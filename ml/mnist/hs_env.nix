# https://nixos.wiki/wiki/FAQ/Pinning_Nixpkgs

let
  
  nixpkgs_from_github = import (builtins.fetchGit {
    # Descriptive name to make the store path easier to identify
    name = "nixos-unstable-2019-07-05";
    url = https://github.com/nixos/nixpkgs/;
    ref = "nixos-unstable";
    # Commit hash for nixos-unstable as of 2019-07-05
    # `git ls-remote https://github.com/nixos/nixpkgs-channels nixos-unstable`
    rev = "73392e79aa62e406683d6a732eb4f4101f4732be";
  }) {};

  nixpkgs = import /home/james/big/work/nixpkgs {};

in

  # Doesn't work: at least some of these are marked as broken.
  nixpkgs.haskellPackages.ghcWithPackages (self: [
    self.numhask
    self.numhask-array
    self.numhask-prelude
  ])

import Data.Fin

%default total

data PuzzleOfSize : Nat -> Nat -> Type where
  Clues : (Fin height -> Fin width -> (Maybe Nat)) -> PuzzleOfSize height width

data UncheckedSolutionOfSize : Nat -> Nat -> Type where
  UncheckedSolution : (horizontalLines : Fin (S height) -> Fin width -> Bool) ->
             (verticalLines : Fin height -> Fin (S width) -> Bool) ->
             UncheckedSolutionOfSize height width

succDecider : {n : Nat} -> {t : Fin (S n) -> Type} ->
              ((x : Fin (S n)) -> Dec (t x)) ->
              (y : Fin n) -> Dec (t (FS y))
succDecider decider y = decider (FS y)

liftFinFn : {n : Nat} -> {t : Fin (S n) -> Type} ->
            ((x : Fin (S n)) -> t x) -> (x : Fin n) ->
            t (FS x)
liftFinFn f x = f (FS x)

finCase : {n : Nat} -> {t : Fin (S n) -> Type} ->
          t FZ -> ((x : Fin n) -> t (FS x)) -> (x : Fin (S n)) ->
          t x
finCase z _ FZ = z
finCase _ s (FS x) = s x

exhaustive : {n : Nat} -> {t : Fin n -> Type} ->
             ((x : Fin n) -> Dec (t x)) ->
             Dec ((x : Fin n) -> t x)
exhaustive {n = Z} decider = Yes (\x => absurd x)
exhaustive {n = S m} {t = t} decider =
  case (decider FZ, exhaustive {n = m} {t = t . FS} (succDecider decider)) of
    (Yes zeroProof, Yes succProof) =>
      Yes (finCase zeroProof succProof)
    (_, No succContradiction) =>
      No (succContradiction . liftFinFn)
    (No zeroContradiction, _) =>
      No (\prf => zeroContradiction (prf FZ))

liftFin : Fin n -> Fin (S n)
liftFin FZ = FZ
liftFin (FS x) = FS (liftFin x)

numTrue : List Bool -> Nat
numTrue = sum . map (\b => if b then 1 else 0)

solutionMatchesClues : PuzzleOfSize height width ->
                       UncheckedSolutionOfSize height width ->
                       Type
solutionMatchesClues (Clues clues)
                     (UncheckedSolution horizontalLines verticalLines) =
  (row : Fin height) -> (column : Fin width) ->
  maybe
    ()
    ( \clue =>
      clue =
      numTrue
      [ horizontalLines (liftFin row) column
      , horizontalLines (FS row) column
      , verticalLines row (liftFin column)
      , verticalLines row (FS column)
      ]
    )
    (clues row column)

solutionIsOneLoop : UncheckedSolutionOfSize height width -> Type
solutionIsOneLoop solution = ?todoSolutionIsOneLoop

solutionIsCorrect : PuzzleOfSize height width ->
                    UncheckedSolutionOfSize height width ->
                    Type
solutionIsCorrect puzzle solution = ( solutionMatchesClues puzzle solution
                                    , solutionIsOneLoop solution
                                    )

data SolutionTo : PuzzleOfSize height width -> Type where
  Solution : (puzzle : PuzzleOfSize height width) ->
             (solution : UncheckedSolutionOfSize height width) ->
             solutionIsCorrect puzzle solution ->
             SolutionTo puzzle

checkSolution : (puzzle : PuzzleOfSize height width) ->
                (solution : UncheckedSolutionOfSize height width) ->
                Dec (solutionIsCorrect puzzle solution)
checkSolution puzzle solution = ?todo

main : IO ()
main = putStrLn "Hello!"

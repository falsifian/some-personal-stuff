import Prelude.File

todoPath : String
todoPath = "/tmp/f"  -- "/home/james/big/SparkleShare/pri/todo"

-- TODO: splitting into lines is suprisingly slow!
main : IO ()
main = do result <- readFile todoPath
          case result of
            Left error => putStrLn ("error: " ++ show error)
            Right content => do putStrLn (content ++ " is content")
                                putStrLn (show (unpack "moo") ++ " is small unpacked")
                                putStrLn (show (unpack content) ++ " is unpacked")
                                putStrLn (show (lines content) ++ " is lines")
                                putStrLn (show (length (lines content)) ++ " lines")
                                putStrLn (show 42 ++ " is 42")

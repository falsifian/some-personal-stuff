module Term (Term(..)) where

data Term = BaseType Int
          | Variable Int
          | Lambda Term Term
          | ForAll Term Term
          | Apply Term Term

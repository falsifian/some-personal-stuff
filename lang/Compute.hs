module Compute where

import Context
import Term

data ComputeResult = ComputeResult
                     { normalizedValue :: Term
                     , normalizedType :: Term
                     , 

addToVars :: Int -> Term -> Term
addToVars x (Variable y) = Variable (x + y)
addToVars x (Lambda a b) = Lambda (AddToVars x a) (AddToVars x b)
addToVars x (ForAll a b) = ForAll (AddToVars x a) (AddToVars x b)
addToVars x (Apply a b) = Apply (AddToVars x a) (AddToVars x b)

normalizeAndComputeType :: Context -> Term -> Either String (Term, Term)
normalizeAndComputeType _context bt@(BaseType x) = return (bt, BaseType (1 + x))
normalizeAndComputeType context@(Context c) (Variable x) | length c > x = normalizeAndComputeType context (addToVars (1 + x) (c !! x))
                                                         | otherwise = Left "Var index too high"
normalizeAndComputeType context@(Context c) (Lambda t x) =
  do (t', _) <- normalizeAndComputeType context t
     (x', xt) <- normalizeAndComputeType (Context (t' : c)) x
     return (Lambda t' x', ForAll t' xt)
normalizeAndComputeType context@(

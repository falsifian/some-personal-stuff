module Context (Context(..)) where

import Term

newtype Context = Context [Term]

-- TODO use sqlite or something

module StringStore (f) where

import qualified Data.ByteString as ByteString
import qualified Data.Maybe as Maybe
import qualified Data.Set as Set
import qualified DownwardStore
import qualified FactSet
import qualified System.Directory as Directory
import System.FilePath ((</>))
import qualified System.Random as Random

f :: FilePath -> DownwardStore.T (FactSet.T ByteString.ByteString)
f path =
  DownwardStore.T
  { DownwardStore.read = do children <- Directory.getDirectoryContents path
                            childContents <- sequence [getRegularFileContents (path </> child) | child <- children]
                            return (FactSet.T (Set.fromList (Maybe.catMaybes childContents)))
  , DownwardStore.write = \ (FactSet.T facts) ->
       let writeString s = do name <- Random.randomRIO (0, 2^256) :: IO Integer
                              ByteString.writeFile (path </> show name) s
       in sequence_ [writeString s | s <- Set.toList facts]
  }

getRegularFileContents :: FilePath -> IO (Maybe ByteString.ByteString)
getRegularFileContents path = do isFile <- Directory.doesFileExist path
                                 if isFile
                                   then do content <- ByteString.readFile path
                                           return (Just content)
                                   else return Nothing

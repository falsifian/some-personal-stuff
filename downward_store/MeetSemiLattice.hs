module MeetSemiLattice (C(meet)) where

class Ord a => C a where
  meet :: a -> a -> a

{-# LANGUAGE GADTs #-}

module FactSet(T(T, unT)) where

import qualified Data.Set as Set
import qualified MeetSemiLattice

data T a = Ord a => T {unT :: Set.Set a}

instance Eq (T a) where
  T x == T y = x == y

instance Ord (T a) where
  -- TODO

instance MeetSemiLattice.C (T a) where
  meet (T x) (T y) = T (Set.union x y)

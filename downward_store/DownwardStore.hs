{-# LANGUAGE GADTs #-}

module DownwardStore (T(T, read, write)) where

import qualified MeetSemiLattice

data T a = MeetSemiLattice.C a => T { read :: IO a, write :: a -> IO () }

module TodoValue (TodoValue(CAD), parse) where

import Control.Monad
import Control.Monad.Error (MonadError(throwError), Error(strMsg))
import qualified Data.List as List

newtype TodoValue = CAD Double deriving (Eq, Ord, Show)

parse :: (Error e, MonadError e m) => String -> m TodoValue
parse s = case stripSuffix " CAD" s of
            Nothing -> throwError (strMsg ("Bad TodoValue suffix: " ++ show s))
            Just v -> liftM CAD (readOrError v)

readOrError :: (Error e, MonadError e m, Read a) => String -> m a
readOrError s = case [x | (x, t) <- reads s, null t] of
                    (h : _) -> return h
                    [] -> throwError (strMsg ("Could not parse " ++ show s))

stripSuffix :: Eq a => [a] -> [a] -> Maybe [a]
stripSuffix suffix x = fmap reverse (List.stripPrefix (reverse suffix) (reverse x))

{-# LANGUAGE DuplicateRecordFields, OverloadedLabels #-}

module ParseTodoFile (parseTodoFile) where

import Control.Monad (foldM, liftM)
import Control.Monad.Error (MonadError(throwError), Error(strMsg))
import qualified Data.List as List
import qualified Data.Maybe as Maybe
import DateString
import TodoItem (TodoItem(..))
import qualified TodoItem
import qualified TodoValue

-- Properties to be applied to items in the file from the current point onward.
data Context = Context {plannedAfter :: Maybe DateString}

assertEqual :: Eq a => String -> a -> a -> ()
assertEqual message x y | x == y = ()
                        | otherwise = error message

-- The file starts with this context.
defaultContext = Context Nothing

parsePlannedAfterDateString :: String -> DateString
parsePlannedAfterDateString = parseDateStringWithDefault "9999-99-99"

knownUnimplementedAnnotations :: [String]
knownUnimplementedAnnotations = ["after", "duration", "needs", "value"]

applyAnnotation :: (Error e, MonadError e m) => String -> TodoItem -> m TodoItem
applyAnnotation annotation item =
  case parseAnnotation annotation of
    ("daily value", valueString) ->
        if Maybe.isJust (dailyValue item)
        then throwError (strMsg "duplicate daily value")
        else do value <- TodoValue.parse valueString
                return item {dailyValue = Just value}
    ("after", valueString) ->
        return (item {TodoItem.plannedAfter = Just (parsePlannedAfterDateString valueString)})
    (name, _vale) -> if elem name knownUnimplementedAnnotations
                     then return item
                     else throwError (strMsg ("Annotation not understood: " ++ annotation))

parseAnnotation :: String -> (String, String)
parseAnnotation x = case List.span (/= ':') x of
                        (_, "") -> (x, "")
                        (h, ':' : t) -> (h, t)

findSubstringIndex :: String -> String -> Maybe Int
findSubstringIndex target text = List.findIndex (List.isPrefixOf target) (List.tails text)

getAnnotations :: (Error e, MonadError e m) => String -> m [String]
getAnnotations [] = return []
getAnnotations l@(_ : t)
  | List.isPrefixOf "}@" l = throwError (strMsg "unexpected }@")
  | otherwise = case List.stripPrefix "@{" l of
                  Nothing -> getAnnotations t
                  Just t' ->
                    let Just ending = findSubstringIndex "}@" t'
                        nextStart = findSubstringIndex "@{" t'
                    in if Maybe.maybe False (< ending) nextStart
                       then throwError (strMsg "unexpected @{")
                       else let (annotation, '}' : '@' : t'') = List.splitAt ending t' in
                              liftM (annotation :) (getAnnotations t'')

isTopLevelLine :: String -> Bool
isTopLevelLine line =
  not (startsWith " " line || startsWith "-" line) || startsWith "- " line

endsWith :: String -> String -> Bool
endsWith suffix x = suffix == drop (max 0 (length x - length suffix)) x

startsWith :: String -> String -> Bool
startsWith prefix x = prefix == take (length prefix) x

parseLines :: (Error e, MonadError e m) => Context -> [(Int, String)] -> m [TodoItem]
parseLines _context [] = return []
parseLines context ((hLine, h) : t)
  | startsWith "After " h =
    let r = drop (length "After ") h
        (date, colon) = splitAt (length r - 1) r
        newPlannedAfter = parsePlannedAfterDateString date in
    seq (assertEqual (show colon ++ " should be \":\". Line: " ++ show h) ":" colon) $
    if maybe False (newPlannedAfter <=) (ParseTodoFile.plannedAfter context)
    then throwError (strMsg ("Dates in lines starting with \"After\" are not strictly increasing: " ++ show newPlannedAfter ++ " follows " ++ show (Maybe.fromJust (ParseTodoFile.plannedAfter context))))
    else parseLines (context {plannedAfter = Just newPlannedAfter}) t
  | startsWith "- " h =
    let (thisItem, rest) = span (not . isTopLevelLine . snd) t in
    do firstItem <- parseTodoItemWithContext hLine context (unlines (h : map snd thisItem))
       rest <- parseLines context rest
       return (firstItem : rest)
  | null h || startsWith "After " h || endsWith ":" h = parseLines context t
  | otherwise = throwError (strMsg ("Can't parse line: " ++ h))

parseTodoFile :: (Error e, MonadError e m) => String -> m [TodoItem]
parseTodoFile content = parseLines defaultContext (zip [1..] (lines content))

parseTodoItemWithContext :: (Error e, MonadError e m) => Int -> Context -> String -> m TodoItem
parseTodoItemWithContext lineNumber context text =
    do annotations <- getAnnotations text
       foldM (flip applyAnnotation) (TodoItem {lineNumber = Just lineNumber, plannedAfter = ParseTodoFile.plannedAfter context, text = text, dailyValue = Nothing}) annotations

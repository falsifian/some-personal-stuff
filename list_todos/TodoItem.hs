{-# LANGUAGE RecordWildCards #-}

module TodoItem (TodoItem(..), prettyPrint) where

import Data.List (intersperse)
import Data.Maybe (catMaybes)
import qualified DateString
import TodoValue

data TodoItem =
  TodoItem
  { -- Which line in the todo file the item appears at. If the item spans many
    -- lines, could be any of them. The first line in the file is line 1.
    lineNumber :: Maybe Int
  , -- The date date the item is marked in the todo file as being "after".
    -- Could mean the item can't be done before then, or just that for planning
    -- purposes I decided to postpone it until then.
    plannedAfter :: Maybe DateString.DateString
  , text :: String
  , dailyValue :: Maybe TodoValue
  } deriving (Eq, Ord, Show)

prettyPrint :: TodoItem -> String
prettyPrint (TodoItem {..}) =
     "Item"
  ++ prettyPrintList
     ( catMaybes
       [ fmap (\x -> "after " ++ DateString.asString x) plannedAfter
       , fmap (\x -> "line " ++ show x) lineNumber
       ]
     )
  ++ "\n"
  ++ indentText text

prettyPrintList :: [String] -> String
prettyPrintList [] = ""
prettyPrintList x@(_:_) = " [" ++ concat (intersperse ", " x) ++ "]:"

indentText :: String -> String
indentText = unlines . map ("    " ++) . lines

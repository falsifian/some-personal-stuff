{-# LANGUAGE FlexibleContexts, NamedWildCards, PartialTypeSignatures #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Main where

import Control.Monad (liftM)
import qualified Data.List
import qualified Data.Ord
import qualified Data.Time.LocalTime
import qualified DateString
import ParseTodoFile
import TodoItem

todoFilePath = "/home/james/org/sparkle-pri/todo"

timeZone :: Data.Time.LocalTime.TimeZone
timeZone = Data.Time.LocalTime.hoursToTimeZone (-7)

todoImportance :: Ord _importance => TodoItem -> _importance
todoImportance x = (Data.Ord.Down (plannedAfter x), fmap Data.Ord.Down (lineNumber x))

compareTodoImportance :: TodoItem -> TodoItem -> Ordering
compareTodoImportance x y = compare (todoImportance x) (todoImportance y)

main :: IO ()
main = do today <- DateString.today timeZone
          allTodoItems <- readFile todoFilePath >>= parseTodoFile
          let nextTodoItems = reverse (Data.List.sortBy compareTodoImportance [item | item <- allTodoItems, maybe True (\date -> date < today) (plannedAfter item)])
          let numNextTodoItems = length nextTodoItems
          putStrLn (show numNextTodoItems ++ " items ready now")
          putStr ("Next items:\n" ++ unlines (map (("\n" ++) . prettyPrint) (take 5 nextTodoItems)))

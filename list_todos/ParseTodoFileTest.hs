-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

{-# LANGUAGE MultiParamTypeClasses #-}

module ParseTodoFileTest where

import Control.Monad (liftM)
import Control.Monad.Error (Error, MonadError(catchError, throwError))
import DateString (DateString(..))
import ParseTodoFile
import Test.Framework
import qualified TodoItem
import TodoValue (TodoValue(CAD))

data UnitError = UnitError

instance Error UnitError where

data ErrorOr a = Error | NoError a deriving (Eq, Show)

instance Functor ErrorOr where
  fmap _ Error = Error
  fmap f (NoError x) = NoError (f x)

instance Applicative ErrorOr where
  pure = NoError
  NoError f <*> NoError x = NoError (f x)
  _ <*> _ = Error

instance Monad ErrorOr where
  Error >>= _ = Error
  (NoError x) >>= f = f x

instance MonadError UnitError ErrorOr where
  catchError Error f = f UnitError
  catchError x@(NoError _) _ = x
  throwError _ = Error

test_emptyFileGivesEmptyList :: IO ()
test_emptyFileGivesEmptyList = parseTodoFile "" >>= assertEqual []

test_splitsDistinctItems :: IO ()
test_splitsDistinctItems =
  do items <- parseTodoFile "- a\n- b\n-- b extra line\n- c\n"
     assertEqual ["- a\n", "- b\n-- b extra line\n", "- c\n"] (map TodoItem.text items)

test_addsTrailingNewline :: IO ()
test_addsTrailingNewline =
  do items <- parseTodoFile "- eat lunch"
     assertEqual ["- eat lunch\n"] (map TodoItem.text items)

test_storesLineNumber :: IO ()
test_storesLineNumber =
  do items <- parseTodoFile "- a\n-- a\n- b\n- c"
     assertEqual [Just 1, Just 3, Just 4] (map TodoItem.lineNumber items)

test_storesMostRecentPlannedAfterDate :: IO ()
test_storesMostRecentPlannedAfterDate =
  do items <- parseTodoFile "- a\nAfter 2035-01-04:\n- a\n- a\nAfter 2099-01-01:\n- a\nAfter 3000-03-04:\n"
     assertEqual [Nothing, Just (DateString "2035-01-04"), Just (DateString "2035-01-04"), Just (DateString "2099-01-01")] (map TodoItem.plannedAfter items)

test_fillsMissingDatePartsInAfter :: IO ()
test_fillsMissingDatePartsInAfter =
  do yearOnly <- parseTodoFile "After 2017:\n- a\n"
     assertEqual 1 (length yearOnly)
     assertBool (map TodoItem.plannedAfter yearOnly >= [Just (DateString "2017-12-31")])
     yearAndMonthOnly <- parseTodoFile "After 2020-05:\n- a\n"
     assertEqual 1 (length yearAndMonthOnly)
     assertBool (map TodoItem.plannedAfter yearAndMonthOnly >= [Just (DateString "2020-05-31")])

test_afterAnnotationOverridesAfterLine :: IO ()
test_afterAnnotationOverridesAfterLine =
  do items <- parseTodoFile "- a @{after:2018-01-01}@\nAfter 2018-02-02:\n- b @{after:2018-03-03}@\n- c\n"
     assertEqual [ Just (DateString "2018-01-01"), Just (DateString "2018-03-03")
                 , Just (DateString "2018-02-02")
                 ]
                 (map TodoItem.plannedAfter items)

test_fillsMissingDatePartsInAfterAnnotation :: IO ()
test_fillsMissingDatePartsInAfterAnnotation =
  do [Just yearAndMonth, Just yearOnly] <-
       liftM (map TodoItem.plannedAfter) $
       parseTodoFile "- a @{after:2018-01}@\n- b @{after:2019}@\n"
     assertBool (yearAndMonth >= DateString "2018-01-31")
     assertBool (yearAndMonth < DateString "2018-02-01")
     assertBool (yearOnly >= DateString "2019-12-31")
     assertBool (yearOnly < DateString "2020-01-01")

test_parsesDailyValue :: IO ()
test_parsesDailyValue =
  do do items <- parseTodoFile "- item\n"
        assertEqual [Nothing] (map TodoItem.dailyValue items)
     do items <- parseTodoFile "- item @{daily value:56 CAD}@\n"
        assertEqual [Just (CAD 56)] (map TodoItem.dailyValue items)
     -- TODO: Assert failure when badly formatted daily value
     -- TODO: Assert failure when there's more than one daily value.

test_throwsErrorOnUnknownAnnotation :: IO ()
test_throwsErrorOnUnknownAnnotation =
  do assertEqual Error (parseTodoFile "- item @{bad annotation}@")
     assertEqual Error (parseTodoFile "- item @{another:5}@")

main :: IO ()
main = htfMain htf_thisModulesTests

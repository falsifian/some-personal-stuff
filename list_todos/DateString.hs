module DateString (DateString(..), parseDateStringWithDefault, today) where

import qualified Data.Time.Clock
import qualified Data.Time.Format
import qualified Data.Time.LocalTime

-- Should be a date in "YYYY-MM-DD" format. Not necessarily validated.
newtype DateString = DateString {asString :: String} deriving (Eq, Ord, Show)

parseDateStringWithDefault :: String -> String -> DateString
parseDateStringWithDefault defaultParts string =
  DateString (string ++ (drop (length string) defaultParts))

today :: Data.Time.LocalTime.TimeZone -> IO DateString
today timeZone = do time <- Data.Time.Clock.getCurrentTime
                    return (DateString (Data.Time.Format.formatTime Data.Time.Format.defaultTimeLocale "%Y-%m-%d" (Data.Time.LocalTime.utcToLocalTime timeZone time)))

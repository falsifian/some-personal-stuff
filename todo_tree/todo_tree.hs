module Main where

import qualified Data.MultiSet as MultiSet
import Data.Time.Clock (UTCTime)
import qualified ReadFromFileTree
import qualified System.Environment

type Todos = MultiSet.MultiSet Todo

data Requirement =
    RequiresSecondsOfWork Double
  | RequiresTimeIsAfter UTCTime

data Value =
    WorthConstant Double
  | WorthConstantBefore UTCTime Double

data Todo =
  Todo
  { outcome :: String
  , requirements :: MultiSet.MultiSet Requirement
  , value :: Value
  }

readRequirement :: ReadFromFileTree.T Requirement
readRequirement = ReadFromFileTree.sum f
  where f "RequiresSecondsOfWork" = ReadFromFileTree.useRead
        f "RequiresTimeIsAfter" = ReadFromFileTree.utcTime

readValue :: ReadFromFileTree.T Value
readValue = ReadFromFileTree.sum f
  where f "WorthConstant" = ReadFromFileTree.useRead
        f "WorthConstantBefore" =
          ReadFromFileTree.product
          ("deadline", ReadFromFileTree.utcTime)
          ("value", ReadFromFileTree.useRead)
          WorthConstantBefore

readTodo = ReadFromFileTree.product
  ("outcome", ReadFromFileTree.utf8String)
  ("requirements", ReadFromFileTree.multiSetOfChildren readRequirement)
  ("value", readValue)
  Todo

main :: IO ()
main = do [path] <- System.Environment.getArgs
          todos <- readTodo path
          print todos

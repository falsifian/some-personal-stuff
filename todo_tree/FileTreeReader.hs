{-# LANGUAGE GADTs #-}

module FileTreeReader (C(read), ReadResult(DirectoryWithChildren, FileWithContent)) where

-- Files and directories with names starting with "." are ignored.

import qualified Data.ByteString as ByteString
import qualified System.Directory as Directory

data ReadResult where
  DirectoryWithChildren :: [String] -> ReadResult
  FileWithContent :: ByteString.ByteString -> ReadResult

class C t where
  read :: FilePath -> t ReadResult
  
instance C IO where
  read path = do isFile <- Directory.doesFileExist path
                 if isFile
                   then do content <- ByteString.readFile path
                           return (FileWithContent content)
                   else do children <- Directory.getDirectoryContents path
                           return (DirectoryWithChildren [child | child <- children, not (hidden child)])
                             where hidden [] = False
                                   hidden ('.' : _) = True
                                   hidden (_ : _) = False

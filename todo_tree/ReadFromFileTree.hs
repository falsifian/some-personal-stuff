{-# LANGUAGE DeriveDataTypeable, RankNTypes #-}

module ReadFromFileTree
( T
, WrongFileTypeException(..)
, multiSetOfChildren
, product
, ReadFromFileTree.sum
, useRead
, utf8String
, utcTime
) where

import qualified Control.Monad as Monad
import qualified Control.Monad.Catch
import qualified Data.Encoding as Encoding
import qualified Data.Encoding.UTF8 as Encoding.UTF8
import qualified Data.List as List
import qualified Data.MultiSet as MultiSet
import Data.Time.Clock (UTCTime)
import Data.Typeable (Typeable)
import qualified FileTreeReader
import System.FilePath ((</>))
import Text.Printf (printf)

type T a = (Control.Monad.Catch.MonadThrow m, FileTreeReader.C m) => FilePath -> m a

data BadFormatException = BadFormatException String deriving (Show, Typeable)
data WrongFileTypeException = UnexpectedDirectoryException | UnexpectedRegularFileException deriving (Show, Typeable)

instance Control.Monad.Catch.Exception BadFormatException
instance Control.Monad.Catch.Exception WrongFileTypeException

multiSetOfChildren :: Ord a => T a -> T (MultiSet.MultiSet a)
multiSetOfChildren read path =
  do result <- FileTreeReader.read path
     case result of
       FileTreeReader.FileWithContent _ -> Control.Monad.Catch.throwM UnexpectedRegularFileException
       FileTreeReader.DirectoryWithChildren children ->
         do elements <- Monad.sequence [read (path </> child) | child <- children]
            return (MultiSet.fromList elements)

class ProductHelperClass a where
  product :: a

instance ProductHelperClass (a -> T a) where
  product f _path = return f

instance ProductHelperClass ((String, T a0) -> (String, T a1) -> (a0 -> a1 -> b) -> T b) where
  product (name0, x0) (name1, x1) f path
   | name0 == name1 = error "product requires distinct names"
   | otherwise =
      do result <- FileTreeReader.read path
         case result of
           FileTreeReader.FileWithContent _ -> Control.Monad.Catch.throwM UnexpectedRegularFileException
           FileTreeReader.DirectoryWithChildren children ->
             if List.sort [name0, name1] == List.sort children
             then do y0 <- x0 (path </> name0)
                     y1 <- x1 (path </> name1)
                     return (f y0 y1)
             else Control.Monad.Catch.throwM (BadFormatException "Directory had wrong children.")

sum :: (String -> T a) -> T a
sum f path =
  do result <- FileTreeReader.read path
     case result of
       FileTreeReader.FileWithContent _content ->
         Control.Monad.Catch.throwM UnexpectedRegularFileException
       FileTreeReader.DirectoryWithChildren [childName] ->
         f childName (path </> childName)
       FileTreeReader.DirectoryWithChildren children ->
         Control.Monad.Catch.throwM (BadFormatException (printf "Expected one file but found %d." (length children)))

useRead :: Read a => T a
useRead path =
  do s <- utf8String path
     return (read s)  -- TODO: handle errors

utf8String :: T String
utf8String path =
  do result <- FileTreeReader.read path
     case result of
       FileTreeReader.FileWithContent content ->
         case Encoding.decodeStrictByteStringExplicit (Encoding.UTF8.UTF8Strict) content of
           Left exception -> Control.Monad.Catch.throwM exception  -- TODO: wrap?
           Right decoded -> return decoded
       FileTreeReader.DirectoryWithChildren _ ->
         Control.Monad.Catch.throwM UnexpectedDirectoryException

utcTime :: T UTCTime
utcTime = useRead  -- TODO: rethink

module Lib where

data FundInfo =
  FI
  { name :: String
  , price :: Double
  , held :: Double  -- number of shares
  , weight :: Double
  }

data FundInfo1 =
  FI1
  { name1 :: String
  , price1 :: Double
  , value1 :: Double  -- USD
  , weight1 :: Double
  }

computeTargetUSDAmounts :: [FundInfo] -> [(String, Double)]
computeTargetUSDAmounts funds =
  let totalHeld = sum [price f * held f | f <- funds]
      unnormalizedTargets = [(name f, weight f * sqrt (price f)) | f <- funds]
      totalUnnormalizedTarget = sum [t | (_name, t) <- unnormalizedTargets]
      targets = [(name, t * totalHeld / totalUnnormalizedTarget) | (name, t) <- unnormalizedTargets]
  in targets

computeWeights :: [FundInfo1] -> [(String, Double)]
computeWeights funds =
  let unnorm :: [(String, Double)]
      unnorm = [(name1 f, value1 f / sqrt (price1 f)) | f <- funds]
      total :: Double
      total = sum [w | (_name, w) <- unnorm]
  in [(name, w / total) | (name, w) <- unnorm]


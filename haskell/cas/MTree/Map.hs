module MTree.Map
(   MOrder
,   delete
,   empty
,   fold
,   insert
,   MTree.Map.lookup
,   size
) where

import qualified Data.ByteString as BS
import qualified MTree.BalancedBinaryTree as BBT
import MTree.MTree
import qualified MTree.Set as Set
import Monad

type MOrder a m = a -> a -> m Ordering

empty :: MTree node m => m node
empty = Set.empty

fold :: MTree node m => (a -> node -> node -> m a) -> a -> node -> m a
fold f =
    let f' acc node =
            do (_label, [key, value]) <- destruct node
               f acc key value
    in Set.fold f'

delete :: MTree node m => MOrder node m -> node -> node -> m node
delete order key root =
    let compareTo node =
            do (_label, [key', _value]) <- destruct node
               order key key'
    in
    liftM fst $ BBT.adjust root compareTo (return . const (Nothing, ()))

-- TODO:  This should directly use BBT.adjust instead of using delete and Set.insert.
insert :: MTree node m => MOrder node m -> node -> node -> node -> m node
insert order key value root =
    do root' <- delete order key root
       pair <- construct BS.empty [key, value]
       Set.insert (keyOrder order) pair root'

lookup :: MTree node m => MOrder node m -> node -> node -> m (Maybe node)
lookup order key root =
    let compareTo node =
            do (_label, [key', _value]) <- destruct node
               order key key'
    in
    do found <- BBT.find root compareTo
       case found of
           Nothing -> return Nothing
           Just node -> do (_label, [_key, value]) <- destruct node
                           return (Just value)

size :: MTree node m => node -> m Int
size = Set.size

keyOrder :: MTree node m => MOrder node m -> MOrder node m
keyOrder order a b =
    do (_label, [aKey, _value]) <- destruct a
       (_label, [bKey, _value]) <- destruct b
       order aKey bKey

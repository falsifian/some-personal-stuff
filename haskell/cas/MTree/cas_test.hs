module Main where

import qualified CAS
import Char
import qualified Data.ByteString.Char8 as BSC
import Hash
import MTree.CAS as CASTree
import MTree.MTree
import Maybe
import Monad

test_0 :: CAS.Op String

test_0 =
	do	a <- construct (BSC.pack "a") [] :: CAS.Op CASTree.T
		b <- construct (BSC.pack "b") []
		c <- construct (BSC.pack "c") []
		d <- construct (BSC.pack "d") [a, b]
		e <- construct (BSC.pack "e") [c, d]
		liftM (flip id "") (dump e)

tests :: [CAS.Op String]
tests = [test_0]

main :: IO ()
main = sequence_ [putStrLn ("test " ++ show i ++ "\n") >> CAS.run test "dag_store" >>= putStr | (i, test) <- zip [0 ..] tests]

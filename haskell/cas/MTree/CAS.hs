{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module MTree.CAS
( T(..)
, gc
, consLabelHash
, desLabelHash
, getLabelHash
) where

import qualified CAS
import CAS.Hash
import Control.Monad
import Control.Monad.Error.Class
import Control.Monad.Trans (MonadIO)
import qualified Data.ByteString as BS
import qualified Data.Set as Set
import MTree.MTree

newtype T = C { getHash :: SHA256 } deriving (Eq, Ord)

instance (Error e, MonadError e m, MonadIO m) => MTree T (CAS.Op m) where
    construct label children =
        do (labelHash, _existed) <- CAS.add label
           consLabelHash labelHash children
    destruct x =
        do (labelHash, children) <- desLabelHash x
           label <- CAS.read labelHash
           return (label, children)
    getChildren x =
        do (labelHash, children) <- desLabelHash x
           return children

gc :: (Error e, MonadError e m, MonadIO m) => [T] -> CAS.Op m ()
gc roots =
    let mark [] marks = CAS.listAll >>= sweep marks
        mark (root : roots) marks
            | Set.member (getHash root) marks = mark roots marks
            | True =
                  do (labelHash, children) <- desLabelHash root
                     mark (children ++ roots) (Set.insert labelHash (Set.insert (getHash root) marks))
        sweep _marks [] = return ()
        sweep marks (hash : hashes) =
            do (if Set.member hash marks then return () else CAS.delete hash)
               sweep marks hashes
    in mark roots Set.empty

-- consLabelHash is like MTree.construct, but takes the hash of the label
-- rather than the label itself.
consLabelHash :: MonadIO m => SHA256 -> [T] -> CAS.Op m T
consLabelHash label children =
    liftM (C . fst) $ CAS.add $ BS.pack $
    concat (map sha256ToBytes (label : map getHash children))

-- desLabelHash is like MTree.destruct, but returns the hash of the label
-- rather than the label itself.
desLabelHash :: (Error e, MonadError e m, MonadIO m) => T -> CAS.Op m (SHA256, [T])
desLabelHash node =
    do (label : children) <- (CAS.read (getHash node)) >>= parse_node
       return (label, map C children)

getLabelHash :: (Error e, MonadError e m, MonadIO m) => T -> CAS.Op m SHA256
getLabelHash = liftM fst . desLabelHash

parse_node :: (Error e, MonadError e m) => BS.ByteString -> m [SHA256]
parse_node content =
    let splitUp acc [] = return (reverse acc)
        splitUp acc s = let (a, b) = splitAt 32 s in
          do h <- bytesToSHA256 a
             splitUp (h : acc) b
    in splitUp [] (BS.unpack content)

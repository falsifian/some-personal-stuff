{-	Based on the same stuff as ghc's Data.Map:
	*  Stephen Adams, "Efficient sets: a balancing act", Journal of Functional Programming 3(4):553-562, October 1993, http://www.swiss.ai.mit.edu/~adams/BB/.
	* J. Nievergelt and E.M. Reingold, "Binary search trees of bounded balance", SIAM journal of computing 2(1), March 1973. 
-}

module MTree.BalancedBinaryTree
(   adjust
,   empty
,   find
,   fold
,   getRange
,   size
,   MTree.BalancedBinaryTree.dump
,   MTree.BalancedBinaryTree.verify
)
where

import qualified Data.ByteString as BS
import qualified Embed.ByteString as EBS
import Embed.Embed
import MTree.MTree
import Maybe
import Monad

data D node =
    Leaf
  | Node Int node node node

max_unbalance :: Int
max_unbalance = 2

dEmbedding :: Embedding BS.ByteString (Maybe Int)
dEmbedding = EBS.maybe (EBS.integral)

construct_node :: MTree node m => D node -> m node
construct_node Leaf = construct (embed dEmbedding Nothing) []
construct_node (Node s x l r) = construct (embed dEmbedding (Just s))
                                          [x, l, r]

destruct_node :: MTree node m => node -> m (D node)
destruct_node node = flip liftM (destruct node) $ \ (label, children) ->
    case (unembed dEmbedding label, children) of
        (Just Nothing, []) -> Leaf
        (Just (Just s), [x, r, l]) -> Node s x r l

adjust :: MTree node m => node -> (node -> m Ordering) -> (Maybe node -> m (Maybe node, result)) -> m (node, result)
adjust root order update =
    let separate_leftmost root_val left right = destruct_node left >>= \ x -> case x of
            Leaf -> return (root_val, right)
            Node _ left_val left_l left_r ->
                do (leftmost, left') <- separate_leftmost left_val left_l left_r
                   root <- cons_rebalance root_val left' right
                   return (leftmost, root)
    in
    destruct_node root >>= \ x -> case x of
        Leaf -> update Nothing >>= \ x -> case x of
                    (Nothing, r) -> return (root, r)
                    (Just v, r) -> do root' <- cons_s v root root
                                      return (root', r)
        Node s root_val left right -> order root_val >>= \ c -> case c of
            LT -> do (left', r) <- adjust left order update
                     finalRoot <- cons_rebalance root_val left' right
                     return (finalRoot, r)
            EQ -> update (Just root_val) >>= \ x -> case x of
                  (Nothing, r) -> destruct_node right >>= \ x -> case x of
                      Leaf -> return (left, r)
                      Node _ right_root_val right_l right_r ->
                          do (r_leftmost, right') <- separate_leftmost right_root_val right_l right_r
                             finalRoot <- cons_rebalance r_leftmost left right'
                             return (finalRoot, r)
                  (Just v, r) -> do finalRoot <- construct_node (Node s v left right)
                                    return (finalRoot, r)
            GT -> do (right', r) <- adjust right order update
                     finalRoot <- cons_rebalance root_val left right'
                     return (finalRoot, r)

empty :: MTree node m => m node
empty = construct_node Leaf

find :: MTree node m => node -> (node -> m Ordering) -> m (Maybe node)
find root query =
    destruct_node root >>= \ x -> case x of
        Leaf -> return Nothing
        Node size root_val left right -> query root_val >>= \ c -> case c of
            LT -> find left query
            EQ -> return (Just root_val)
            GT -> find right query

fold :: MTree node m => (a -> node -> m a) -> a -> node -> m a
fold f x root = destruct_node root >>= \y -> case y of
    Leaf -> return x
    Node _ root_val left right ->
        f x root_val >>= flip (fold f) left >>= flip (fold f) right

getRange :: MTree node m
         => (node -> m Ordering) -> (node -> m Ordering) -> node -> m [node]
getRange start end =
    let getRange' acc root = destruct_node root >>= \ x -> case x of
            Leaf -> return acc
            Node _ v l r ->
                do cmpStartV <- start v
                   cmpEndV <- end v
                   acc' <- if LT /= cmpEndV then getRange' acc r else return acc
                   let acc'' = if LT /= cmpEndV && GT /= cmpStartV
                                   then v : acc'
                                   else acc'
                   if GT /= cmpStartV then getRange' acc'' l else return acc''
    in  getRange' []

size :: MTree node m => node -> m Int
size t = flip liftM (destruct_node t) $ \ x -> case x of
	Leaf -> 0
	Node size _ _ _ -> size

dump :: MTree node m => (node -> m ShowS) -> node -> m ShowS
dump shows_val =
    let d prefix root = destruct_node root >>= \ x -> case x of
            Leaf -> return ((prefix ++ "+L\n") ++)
            Node size root_val left right ->
                do ls <- d (prefix ++ " ") left
                   rs <- d (prefix ++ " ") right
                   s <- shows_val root_val
                   return (((prefix ++ "+(") ++) . shows size . (", " ++) . s . (")\n" ++) . ls . rs)
    in	d ""

verify :: MTree node m => (node -> node -> m Ordering) -> node -> m ()
verify order = liftM (const ()) . verify' order Nothing Nothing

verify' :: MTree node m => (node -> node -> m Ordering) -> Maybe node -> Maybe node -> node -> m Int
verify' order min_key max_key root =
    destruct_node root >>= \ x -> case x of
        Leaf -> return 0
        Node size root_val left right ->
            do True <- in_range order min_key max_key root_val
               left_size <- verify' order min_key (Just root_val) left
               right_size <- verify' order (Just root_val) max_key right
               let True = left_size + right_size < 2 || (left_size * 2 >= right_size && right_size * 2 >= left_size)
               let True = size == succ (left_size + right_size)
               return (succ (left_size + right_size))

in_range :: Monad m => (k -> k -> m Ordering) -> Maybe k -> Maybe k -> k -> m Bool
in_range compare min max k =
    do a <- maybe (return GT) (compare k) min
       b <- maybe (return LT) (compare k) max
       return ((a /= LT) && (b /= GT))

cons_s :: MTree node m => node -> node -> node -> m node
cons_s root_val left right =
    do ls <- size left
       rs <- size right
       construct_node $ Node (succ (ls + rs)) root_val left right

cons_rebalance :: MTree node m => node -> node -> node -> m node
cons_rebalance root_val left right =
    do ls <- size left
       rs <- size right
       n <- cons_s root_val left right
       if ls + rs < 2
           then return n
           else if rs > max_unbalance * ls
           then
           	do	Node _ _ rl rr <- destruct_node right
           		rls <- size rl
           		rrs <- size rr
           		if (rls < rrs) then single_left n else double_left n
           else if ls > max_unbalance * rs
           then
           	do	Node _ _ ll lr <- destruct_node left
           		lls <- size ll
           		lrs <- size lr
           		if (lrs < lls) then single_right n else double_right n
           else return n

single_right :: MTree node m => node -> m node
single_right root =
    do Node _ root_val left right <- destruct_node root
       Node _ left_val left_l left_r <- destruct_node left
       right' <- cons_s root_val left_r right
       cons_s left_val left_l right'

single_left :: MTree node m => node -> m node
single_left root =
    do Node _ root_val left right <- destruct_node  root
       Node _ right_val right_l right_r <- destruct_node right
       left' <- cons_s root_val left right_l
       cons_s right_val left' right_r

double_right :: MTree node m => node -> m node
double_right root =
    do Node _ a l r <- destruct_node root
       Node _ b ll lr <- destruct_node l
       Node _ c lrl lrr <- destruct_node lr
       l' <- cons_s b ll lrl
       r' <- cons_s a lrr r
       cons_s c l' r'

double_left :: MTree node m => node -> m node
double_left root =
    do Node _ a  l r <- destruct_node root
       Node _ b rl rr <- destruct_node r
       Node _ c rll rlr <- destruct_node rl
       l' <- cons_s a l rll
       r' <- cons_s b rlr rr
       cons_s c l' r'

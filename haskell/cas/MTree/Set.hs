module MTree.Set
(   MOrder
,   delete
,   empty
,   fold
,   getRange
,   insert
,   member
,   size
) where

import qualified MTree.BalancedBinaryTree as BBT
import MTree.MTree
import Maybe
import Monad

type MOrder a m = a -> a -> m Ordering

empty :: MTree node m => m node
empty = BBT.empty

fold :: MTree node m => (a -> node -> m a) -> a -> node -> m a
fold = BBT.fold

delete :: MTree node m => MOrder node m -> node -> node -> m node
delete order old root =
    liftM fst $ BBT.adjust root (order old) (return . const (Nothing, ()))

getRange :: MTree node m
         => MOrder node m -> Maybe node -> Maybe node -> node -> m [node]
getRange order start end =
    let start' = maybe (const (return LT)) order start
        end' = maybe (const (return GT)) order end
    in  BBT.getRange start' end'

-- insert is guaranteed never to compare an existing set entry to itself
-- (unless the entry is present twice in the set).
insert :: MTree node m => MOrder node m -> node -> node -> m node
insert order new root =
    liftM fst $ BBT.adjust root (order new) (return . const (Just new, ()))

member :: MTree node m => MOrder node m -> node -> node -> m Bool
member order x s = liftM isJust $ BBT.find s (order x)

size :: MTree node m => node -> m Int
size = BBT.size

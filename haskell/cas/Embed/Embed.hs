module Embed.Embed
(	Embedding (Embedding, embed, unembed)
,	ListEmbedding (ListEmbedding, list_embed_empty, list_embed_cons, list_embed_des)
,	compose
,	list_embedding_to_embedding
,	Embed.Embed.map
)
where

import Maybe
import Monad

data Embedding a b =
		Embedding
		{	embed :: b -> a
		,	unembed :: a -> Maybe b
		}

data ListEmbedding a b =
		ListEmbedding
		{	list_embed_empty :: a
		,	list_embed_cons :: b -> a -> Maybe a
		,	list_embed_des :: a -> Maybe (Maybe (b, a))
		}

compose :: Embedding a b -> Embedding b c -> Embedding a c
compose e f = Embedding (embed e . embed f) ((>>= unembed f) . unembed e)

list_embedding_to_embedding :: ListEmbedding a b -> Embedding a [b]
list_embedding_to_embedding (ListEmbedding { list_embed_empty = empty, list_embed_cons = cons, list_embed_des = des}) =
	let	embed_ acc [] = acc
		embed_ acc (h : t) = embed_ (fromJust (cons h acc)) t
		unembed_ acc x = case des x of
			Nothing -> Nothing
			Just Nothing -> Just acc
			Just (Just (h, t)) -> unembed_ (h : acc) t
	in	Embedding (embed_ empty . reverse) (liftM reverse . unembed_ [])

map :: Embedding a b -> Embedding [a] [b]
map e =
	Embedding
	{	embed = Prelude.map (embed e)
	,	unembed = sequence . Prelude.map (unembed e)
	}

module Embed.ByteString
(	blocks
,	bool
,	Embed.ByteString.either
,	fixed_length_integral
,	fixed_length_integral_list
,	integral
,	Embed.ByteString.maybe
,	string
,	tuple2
)
where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import Embed.Embed
import List
import Monad

bool :: Embedding BS.ByteString Bool
bool =
	let	embed_ True = BS.singleton 0
		embed_ False = BS.singleton 1
		unembed_ s =
			do	1 <- return (BS.length s)
				case BS.head s of
					0 -> return True
					1 -> return False
					_ -> mzero
	in	Embedding embed_ unembed_

blocks :: Int -> ListEmbedding BS.ByteString BS.ByteString
blocks n =
	let	empty = BS.empty
		cons h t = if n == BS.length h then if 0 == mod (BS.length t) n then Just (BS.append h t) else error "Embed.ByteString.blocks: cons: bad tail" else Nothing
		des x = guard (0 == mod (BS.length x) n) >> return (guard (not (BS.null x)) >> return (BS.splitAt n x))
	in	ListEmbedding empty cons des
		

either :: Embedding BS.ByteString a -> Embedding BS.ByteString b -> Embedding BS.ByteString (Either a b)
either ea eb =
	let	embed_ (Left x) = BS.cons 0 (embed ea x)
		embed_ (Right x) = BS.cons 1 (embed eb x)
		unembed_ s =
			do	(head, tail) <- BS.uncons s
				case head of
					0 -> liftM Left (unembed ea tail)
					1 -> liftM Right (unembed eb tail)
					_ -> mzero
	in	Embedding embed_ unembed_

fixed_length_integral :: Integral a => Int -> Embedding BS.ByteString a
fixed_length_integral length =
	Embedding
	{	embed = BS.pack . take length . unfoldr (\ n -> Just (fromIntegral $ mod n 256, div n 256))
	,	unembed = \ x -> if length == BS.length x then Just $ fst $ foldl (\ (sum, mult) x -> (sum + mult * fromIntegral x, mult * 256)) (0, 1) (BS.unpack x) else Nothing
	}

fixed_length_integral_list :: Integral a => Int -> ListEmbedding BS.ByteString a
fixed_length_integral_list length =
    ListEmbedding
    { list_embed_empty = BS.empty
    , list_embed_cons = \ n s -> case mod (BS.length s) length of 
          0 -> Just ( BS.append (BS.pack (take length (unfoldr (\ n -> Just (fromIntegral $ mod n 256, div n 256)) n))) s)
          _ -> Nothing
    , list_embed_des = \ s -> case mod (BS.length s) length of
          0 -> Just $ case BS.length s of
                   0 -> Nothing
                   _ -> let (h, t) = BS.splitAt length s in Just (fst (foldl (\ (sum, mult) x -> (sum + mult * fromIntegral x, mult * 256)) (0, 1) (BS.unpack h)), t)
          _ -> Nothing
    }

integral :: Integral a => Embedding BS.ByteString a
integral =
	let	embed n =
			let	e a n = if 0 == n then a else e (fromIntegral (mod n 0x100) : a) (div n 0x100)
			in	BS.pack (reverse (e [] n))
		unembed s =
			let	u :: (Integral a) => a -> a -> Int -> a
				u a m i = if BS.length s <= i then a else u (a + m * fromIntegral (BS.index s i)) (0x100 * m) (succ i)
			in	Just (u 0 1 0)
	in	Embedding embed unembed

maybe :: Embedding BS.ByteString a -> Embedding BS.ByteString (Maybe a)
maybe e =
	let	embed_ Nothing = BS.empty
		embed_ (Just x) = BS.cons 0 (embed e x)
		unembed_ s = if 0 == BS.length s
			then Just Nothing
			else
			do	(0, s') <- BS.uncons s
				liftM Just (unembed e s')
	in	Embedding embed_ unembed_

string :: Embedding BS.ByteString String
string = Embedding BSC.pack (Just . BSC.unpack)

tuple2 :: Embedding BS.ByteString a -> Embedding BS.ByteString b -> Embedding BS.ByteString (a, b)
tuple2 a_emb b_emb =
	let	embed_ (x, y) =
			let	xe = embed a_emb x
				ye = embed b_emb y
			in	prepend_n (BS.length xe) (BS.append xe ye)
		unembed_ s =
			let	(n, s') = get_pre_n s
				(xs, ys) = BS.splitAt n s'
			in
			do	x <- unembed a_emb xs
				y <- unembed b_emb ys
				return (x, y)
	in	Embedding embed_ unembed_

prepend_n :: Integral a => a -> BS.ByteString -> BS.ByteString
prepend_n n s =
	let	p a n = if 0x80 > n
			then BS.append (BS.pack (reverse (fromIntegral n : a))) s
			else p (0x80 + fromIntegral (mod n 0x80) : a) (div n 0x80)
	in	p [] n

get_pre_n :: Integral a => BS.ByteString -> (a, BS.ByteString)
get_pre_n s =
	let	g a m i = if 0x80 > BS.index s i
			then (a + m * fromIntegral (BS.index s i), BS.drop (succ i) s)
			else	g (a + m * fromIntegral (BS.index s i - 0x80)) (0x80 * m) (succ i)
	in	g 0 1 0

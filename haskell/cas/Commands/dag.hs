module Main where

import qualified CAS
import CASDAG
import qualified Data.ByteString as BS
import Hash
import Util

add_data_cmd :: [String] -> IO ()
add_data_cmd [casPath] =
	do	dat <- BS.getContents
		run (add_data dat) casPath >>= putStrLn . hash0ToString
add_data_cmd _ = fail "add_data: usage: add_data cas"

gc_cmd :: [String] -> IO ()
gc_cmd (casPath : root_hashes) = CAS.run (gc (map stringToHash0 root_hashes)) casPath
gc_cmd [] = fail "gc: usage: gc cas [root ...]"

make_cmd :: [String] -> IO ()
make_cmd (casPath : data_hash : child_hashes) =
	do	node_hash <- run (make (stringToHash0 data_hash) (map stringToHash0 child_hashes)) casPath
		putStrLn (hash0ToString node_hash)
make_cmd [] = fail "make: usage: add cas data_hash [child_hash ...]"

repr_cmd :: [String] -> IO ()
repr_cmd [casPath, root_hash] = run (repr (stringToHash0 root_hash)) casPath >>= putStr
repr_cmd _ = fail "repr: usage: print cas root"

read_data_cmd :: [String] -> IO ()
read_data_cmd [casPath, data_hash] = run (read_data (stringToHash0 data_hash)) casPath >>= BS.putStr
read_data_cmd _ = fail "read_data: usage: read_node cas hash"

read_node_cmd :: [String] -> IO ()
read_node_cmd [casPath, node_hash] =
	do	(content_hash, child_hashes) <- run (read_node (stringToHash0 node_hash)) casPath
		sequence_ (map (putStrLn . hash0ToString) (content_hash : child_hashes))
read_node_cmd _ = fail "read_node: usage: read_node cas hash"

-- Note: this doesn't check for cycles.  A cycle should be computationally hard to construct.
verify_cmd :: [String] -> IO ()
verify_cmd [casPath, root] = CAS.verify casPath >> CAS.run (verify (stringToHash0 root)) casPath
verify_cmd _ = fail "verify: usage: verify cas root"

main :: IO ()
main =
	let	cmds "add_data" = Just add_data_cmd
		cmds "gc" = Just gc_cmd
		cmds "make" = Just make_cmd
		cmds "read_data" = Just read_data_cmd
		cmds "read_node" = Just read_node_cmd
		cmds "repr" = Just repr_cmd
		cmds "verify" = Just verify_cmd
		cmds _ = Nothing
	in	cmds_main cmds

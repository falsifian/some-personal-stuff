module Main where

import CAS
import CAS.Hash
import Control.Monad
import qualified Data.ByteString as BS
import Data.Char
import Text.Printf
import Util

add_cmd :: [String] -> IO ()
add_cmd [casPath] =
	do	content <- BS.getContents
		(hash, exists) <- run (CAS.add content) casPath
		putStrLn (sha256ToHex hash)
		putStrLn (if exists then "already existed" else "added")
add_cmd _ = fail "add: usage: add <cas>"

delete_cmd :: [String] -> IO ()
delete_cmd [casPath, hash] = run (hexToSHA256 hash >>= CAS.delete) casPath
delete_cmd _ = fail "delete: usage: delete <cas> <hash>"

get_path_cmd :: [String] -> IO ()
get_path_cmd [casPath, hash] =
  run (hexToSHA256 hash >>= CAS.getPath) casPath >>= putStrLn
get_path_cmd _ = fail "read: usage: get_path <cas> <hash>"

read_cmd :: [String] -> IO ()
read_cmd [casPath, hash] =
  run (hexToSHA256 hash >>= CAS.read) casPath >>= BS.putStr
read_cmd _ = fail "read: usage: read <cas> <hash>"

sync_cmd :: [String] -> IO ()
sync_cmd [casPathInner, casPathOuter, i2oStr, o2iStr] =
  let readBool x =
        case map toUpper x of
          "F" -> False
          "N" -> False
          "T" -> True
          "Y" -> True
          "FALSE" -> False
          "NO" -> False
          "TRUE" -> True
          "YES" -> True
      o2i = readBool o2iStr
      i2o = readBool i2oStr
      presentStats sourcePath destPath stats =
        printf "%d bytes in %d strings copied from %s to %s."
        (numBytesCopied stats) (numStringsCopied stats) sourcePath destPath
  in
  do (i2oStats, o2iStats) <- flip run casPathInner $ flip run casPathOuter $ syncWithInner i2o o2i
     when i2o $ putStrLn (presentStats casPathInner casPathOuter i2oStats)
     when o2i $ putStrLn (presentStats casPathOuter casPathInner o2iStats)

verify_cmd :: [String] -> IO ()
verify_cmd [casPath] = verify casPath
verify_cmd _ = fail "verify: usage: verify <cas>"

main :: IO ()
main =
	let	cmds "add" = Just add_cmd
		cmds "delete" = Just delete_cmd
                cmds "get_path" = Just get_path_cmd
		cmds "read" = Just read_cmd
                cmds "sync" = Just sync_cmd
		cmds "verify" = Just verify_cmd
		cmds _ = Nothing
	in	cmds_main cmds

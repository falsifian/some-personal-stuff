module Main where

import CASDAG
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import Directory
import Hash
import IO
import List
import Maybe
import Monad
import System

main :: IO ()
main =
	do	[cas, mode, arg] <- getArgs
		case mode of
			"c" -> create cas arg >>= putStrLn . hash0ToString
			"x" -> extract cas (stringToHash0 arg) -- XXX Refuse to run in a non-empty directory.

create :: CASSpec -> FilePath -> IO Hash0
create cas path =
	let	create_sub :: FilePath -> IO (Maybe (Hash0, Bool, FilePath))
		create_sub "." = return Nothing
		create_sub ".." = return Nothing
		create_sub file = liftM Just $
			let	file_path = path ++ "/" ++ file
			in
			do	is_directory <- doesDirectoryExist file_path
				if is_directory then liftM (\ hash -> (hash, True, file)) (create cas file_path) else
					do	handle <- openFile file_path ReadMode
						content <- BS.hGetContents handle
						hClose handle
						node_hash <- run (add_data content >>= flip make []) cas
						return (node_hash, False, file)
	in
	do	files <- getDirectoryContents path
		children <- liftM (sort . catMaybes) (sequence (map create_sub files))
		index <- return (BSC.pack (unlines [(if is_dir then "d " else "f ") ++ name | (_, is_dir, name) <- children]))
		node_hash <- run (add_data index >>= flip make [hash | (hash, _, _) <- children]) cas
		return node_hash

extract :: CASSpec -> Hash0 -> IO ()
extract cas root_hash =
	do	getDirectoryContents "." >>= \ fs -> when (not (and ["." == f || ".." == f | f <- fs])) (fail "extract: current directory not empty")
		extract' cas root_hash "."

extract' :: CASSpec -> Hash0 -> FilePath -> IO ()
extract' cas root_hash path =
	let	extract_dir hash name =
			do	createDirectory (path ++ "/" ++ name)
				extract' cas hash (path ++ "/" ++ name)
		extract_file hash name =
			do	content <- run (read_node hash >>= read_data . fst) cas
				handle <- openFile (path ++ "/" ++ name) WriteMode
				BS.hPutStr handle content
				hClose handle
	in
	do	(index_hash, child_hashes) <- run (read_node root_hash) cas
		index <- run (read_data index_hash) cas
		sequence_
			[	let [dir_ind, name] = words entry in
				(case dir_ind of "d" -> extract_dir ; "f" -> extract_file) hash name
			|	(entry, hash) <- zip (lines (BSC.unpack index)) child_hashes
			]

module CAS.Hash
(   SHA256(sha256ToBytes)
,   sha256
,   isValidSHA256Hex
,   bytesToSHA256
,   sha256ToHex
,   hexToSHA256
) where

import Control.Monad
import Control.Monad.Error
import qualified Crypto.Hash.SHA256 as SHA256
import qualified Data.ByteString as BS
import Data.ByteString.Internal
import Data.Word
import Foreign.ForeignPtr
import Foreign.Ptr
import Numeric

newtype SHA256 = SHA256 {sha256ToBytes :: [Word8]} deriving (Eq, Ord)

sha256 :: BS.ByteString -> SHA256
sha256 = SHA256 . BS.unpack . SHA256.hash

isValidSHA256Hex :: String -> Bool
isValidSHA256Hex s =
    64 == length s
 && and [c >= '0' && c <= '9' || c >= 'a' && c <= 'f' | c <- s]

bytesToSHA256 :: (Error e, MonadError e m) => [Word8] -> m SHA256
bytesToSHA256 x | 32 == length x = return (SHA256 x)
                | True = throwError $ strMsg "bytesToSHA256: wrong length"

sha256ToHex :: SHA256 -> String
sha256ToHex = (>>= word8ToBigEndianHex) . sha256ToBytes

word8ToBigEndianHex :: Word8 -> String
word8ToBigEndianHex x = showHex (div x 16) $ showHex (mod x 16) ""

hexToSHA256 :: (Error e, MonadError e m) => String -> m SHA256
hexToSHA256 = f []
    where f acc [] = bytesToSHA256 $ reverse acc
          f acc (h0 : h1 : t) = case readHex [h0, h1] of
            [(v, "")] -> f (v : acc) t
            _ -> throwError $ strMsg "hexToSHA256: bad hex value"
          f _ _ = throwError $ strMsg "hexToSHA256: odd number of digits"

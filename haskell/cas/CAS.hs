{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, UndecidableInstances #-}

module CAS (SHA256, Op, SyncStats(..), add, CAS.delete, getPath, exists, listAll, CAS.read, run, syncWithInner, verify) where

{-
	Storage format: a directory, where all the file names are SHA256 hash codes.
-}

import Control.Monad
import Control.Monad.Error.Class
import Control.Monad.Trans
import qualified Data.ByteString as BS
import Data.List
import Data.Maybe
import Data.Monoid
import qualified Data.Set as Set
import CAS.Hash
import System.Directory
import System.FilePath
import System.IO
import Text.Printf
import Text.Regex.Posix
import Util

data Spec =
  SpecFormat0 {fmt0DataDir :: FilePath}
  
data SyncStats =
  SyncStats {numStringsCopied, numBytesCopied :: Integer}
  
instance Monoid SyncStats where
  mempty = SyncStats {numStringsCopied = 0, numBytesCopied = 0}
  mappend a b =
    SyncStats
    { numStringsCopied = numStringsCopied a + numStringsCopied b
    , numBytesCopied = numBytesCopied a + numBytesCopied b
    }

newtype Op m a = Op { opRun :: Spec -> m a }

instance Monad m => Monad (Op m) where
	return = Op . const . return
	Op f >>= g = Op (\ cas -> f cas >>= \ x -> let Op h = g x in h cas)

-- Applicative and Functor instances added 2018-09-22 since Haskell now
-- requires all instances of Monad to also be instances of Applicative and
-- therefore of Functor.

instance Monad m => Functor (Op m) where
    fmap = liftM

instance Monad m => Applicative (Op m) where
    pure = return
    (<*>) = ap
        
instance MonadTrans Op where
  lift = Op . const
  
instance MonadIO m => MonadIO (Op m) where
  liftIO = lift . liftIO
  
instance MonadError e m => MonadError e (Op m) where
  throwError = lift . throwError
  catchError x c = Op $ \spec -> catchError (opRun x spec) (\e -> opRun (c e) spec)
  
-- Adds the string to the CAS, and returns (hash, existed), where hash is the
-- hash of the string, and existed indicates whether the string was already in
-- the CAS before add was called.
--
-- If more than one process calls add at the same time, it is guaranteed that
-- neither process will return before the string is added.  If the string
-- existed before either process called add, then both processes will return
-- existed as True.  Starting from an empty store, it is guaranteed that at
-- least one process will return existed as False when adding a given string.
add :: MonadIO m => BS.ByteString -> Op m (SHA256, Bool)
add content = Op $ \ SpecFormat0 {fmt0DataDir = dataDir} ->
  liftIO $
  do let hash = sha256 content
     let hashStr = sha256ToHex hash
     let targetPath = dataDir </> hashStr
     existed <- doesFileExist targetPath
     when (not existed) $
       do (tempPath, h) <- openTempFile dataDir "temp-"
          BS.hPutStr h content
          hClose h
          renameFile tempPath targetPath
     return (hash, existed)

delete :: MonadIO m => SHA256 -> Op m ()
delete hash =
  Op $ \ SpecFormat0 {fmt0DataDir = dataDir} ->
  liftIO $ removeFile (dataDir </> sha256ToHex hash)

exists :: MonadIO m => SHA256 -> Op m Bool
exists hash = Op $ \ SpecFormat0 {fmt0DataDir = dataDir} ->
  liftIO $ doesFileExist (dataDir </> sha256ToHex hash)
  
getPath :: MonadIO m => SHA256 -> Op m FilePath
getPath hash = Op $ \ SpecFormat0 {fmt0DataDir = dataDir} ->
  return $ dataDir </> sha256ToHex hash

read :: MonadIO m => SHA256 -> Op m BS.ByteString
read hash =
  do path <- getPath hash
     liftIO $
       do handle <- openFile path ReadMode
          content <- BS.hGetContents handle
          hClose handle
          return content

listAll :: (Error e, MonadError e m, MonadIO m) => Op m [SHA256]
listAll =
  let f "." = False
      f ".." = False
      f x = True
  in Op $ \ SpecFormat0 {fmt0DataDir = dataDir} ->
  do dataFileNames <- liftM (filter f) $ liftIO $ getDirectoryContents dataDir
     hashes <- sequence $ map hexToSHA256 dataFileNames
     return $ sort hashes

verify :: FilePath -> IO ()
verify casPath = getSpec casPath >>= \spec ->
  case spec of
    SpecFormat0 {fmt0DataDir = dataDir} ->
      getDirectoryContents dataDir >>= sequence_ . map (verifyFile dataDir)

verifyFile :: FilePath -> FilePath -> IO ()
verifyFile _ "." = return ()
verifyFile _ ".." = return ()
verifyFile dataDir fileName
  | fileName =~ "^temp-"  = hPutStrLn stderr $ printf "Warning: found a temporary file found while verifying CAS: %s" (show fileName)
  | True =
  do h <- openFile (dataDir </> fileName) ReadMode
     content <- BS.hGetContents h
     hClose h
     let hash = sha256 content
     when (fileName /= sha256ToHex hash) (fail ("file named " ++ show fileName ++ " has hash " ++ sha256ToHex hash))

getSpec :: FilePath -> IO Spec
getSpec casPath =
  readFile (casPath </> "format") >>= \formatStr ->
  case formatStr of
    "0" -> do let dataDir = casPath </> "data"
              doesDirectoryExist dataDir >>=
                ( flip unless $ throwError $
                  strMsg "CAS.getSpec:  No data directory found in format 0 CAS."
                )
              return $ SpecFormat0 {fmt0DataDir = dataDir}
    _ -> throwError $ strMsg $ printf "CAS.getSpec:  Unkown format %s." (show formatStr)

run :: MonadIO m => Op m a -> FilePath -> m a
run x casPath =
  do spec <- liftIO $ getSpec casPath
     opRun x spec

syncWithInner :: (Error e, MonadError e m, MonadIO m) => Bool -> Bool -> Op (Op m) (SyncStats, SyncStats)
syncWithInner pullFromInner pushToInner =
  let oneDirection readSource addDest hashes = liftM mconcat $
        sequence
        [ do s <- readSource hash
             (hash', existed) <- addDest s
             when (hash /= hash') $ throwError $ strMsg $ printf "CAS.syncWithInner: expected hash %s but got hash %s" (sha256ToHex hash) (sha256ToHex hash')
             when existed $ throwError $ strMsg $ printf "CAS.syncWithInner: hash %s existed in destination which didn't exist before -- is something else accessing the CAS?" (sha256ToHex hash)
             return $ SyncStats {numStringsCopied = 1, numBytesCopied = fromIntegral $ BS.length s}
        | hash <- Set.toList hashes
        ]
  in
  do outerHashes <- liftM Set.fromList listAll
     innerHashes <- liftM Set.fromList $ lift listAll
     i2oStats <- if pullFromInner
                 then oneDirection (lift . CAS.read) add (Set.difference innerHashes outerHashes)
                 else return mempty
     o2iStats <- if pushToInner
                 then oneDirection CAS.read (lift . add) (Set.difference outerHashes innerHashes)
                 else return mempty
     return (i2oStats, o2iStats)

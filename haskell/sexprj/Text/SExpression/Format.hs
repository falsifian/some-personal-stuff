module Text.SExpression.Format
(   SExprFormat(..)
,   Field(..)
,   readFormatted
,   showFormatted
,   lookupAtom
,   lookupString
,   maybeAtomField
,   reqAtomField
,   maybeStrField
,   reqStrField
) where

import Control.Monad
import Control.Monad.Error
import qualified Data.Map as Map
import Data.Maybe
import Text.Printf
import Text.SExpression

class SExprFormat a where
    sExprFormat :: (Int, [(String, Field a)])
    getInitialFields :: a -> [Value]
    consFromFormat :: (Error e, MonadError e m) => [Value] -> (Map.Map String [Value]) -> m a

data Field a =
    Field
    { fOptional :: Bool
    , fGet :: a -> Maybe [Value]
    }

readFormatted :: (Error e, MonadError e m, SExprFormat a) => [Value] -> m a
readFormatted ss =
    let (nInitialFields, fields) = sExprFormat
        fieldMap = Map.fromList fields
    in
    do when (length ss < nInitialFields) $ throwError $ strMsg "readFormatted: missing initial fields"
       let (initialFields, attribs) = splitAt nInitialFields ss
       let f acc [] =
               do sequence_ [unless (fOptional f || Map.member n acc) $ throwError $ strMsg ("readFormatted: field missing: " ++ show n)| (n, f) <- fields]
                  sequence_ [unless (Map.member n fieldMap) $ throwError $ strMsg ("readFormatted: unknown field: " ++ show n)| (n, _f) <- Map.toList acc]
                  consFromFormat initialFields acc
           f acc (List (Atom fieldName : fieldSs) : attribTail) =
               do when (Map.member fieldName acc) $ throwError $ strMsg ("readFormatted: duplicate field: " ++ show fieldName)
                  f (Map.insert fieldName fieldSs acc) attribTail
           f _acc (attribHead : _) = throwError $ strMsg ("readFormatted: bad attribute format: " ++ show attribHead)
       ret <- f Map.empty attribs
       let _forceFieldMapType = ignore (fGet (head (Map.elems fieldMap)) ret)
       return ret

showFormatted :: SExprFormat a => a -> [Value]
showFormatted x =
    let (_nInitialFields, fields) = sExprFormat
    in getInitialFields x ++ [List (Atom n : ss) | (n, f) <- fields, ss <- maybeToList (fGet f x)]

ignore :: a -> ()
ignore _ = ()

lookupAtom :: (Error e, MonadError e m) => String -> Map.Map String [Value] -> m (Maybe String)
lookupAtom key attribs =
  case Map.lookup key attribs of
    Nothing -> return Nothing
    Just [Atom s] -> return (Just s)
    Just se -> throwError $ strMsg $ printf "lookupAtom: not a singleton atom: %s" (show se)

lookupString :: (Error e, MonadError e m) => String -> Map.Map String [Value] -> m (Maybe String)
lookupString key attribs =
  case Map.lookup key attribs of
    Nothing -> return Nothing
    Just [String s] -> return (Just s)
    Just se -> throwError $ strMsg $ printf "lookupString: not a singleton string: %s" (show se)

maybeAtomField :: (a -> Maybe String) -> Field a
maybeAtomField get =
  Field
  { fOptional = True
  , fGet = \x -> get x >>= \y -> return [Atom y]
  }

reqAtomField :: (a -> String) -> Field a
reqAtomField get =
  Field
  { fOptional = False
  , fGet = \x -> Just [Atom (get x)]
  }

maybeStrField :: (a -> Maybe String) -> Field a
maybeStrField get =
  Field
  { fOptional = True
  , fGet = \x -> get x >>= \y -> return [String y]
  }

reqStrField :: (a -> String) -> Field a
reqStrField get =
  Field
  { fOptional = False
  , fGet = \x -> Just [String (get x)]
  }

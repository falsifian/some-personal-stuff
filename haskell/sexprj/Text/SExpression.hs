module Text.SExpression
(   Value(..)
,   readSExpr
) where

import Data.List

data Value = Atom String | String String | List [Value]

instance Show Value where
    showsPrec _ s = showsSExpr s

readSExpr :: Monad m => String -> m (Value, String)
readSExpr x =
    let whitespace = "\t\n\v\f\r "
        atomChars = "+-_.:=/" ++ ['0'..'9'] ++ ['A'..'Z'] ++ ['a'..'z']

        -- simpleRead parses a single s-expression with trailing
        -- characters but no preceeding whitespace.
        simpleRead :: Monad m => String -> m (Maybe Value, String)
        simpleRead [] = return (Nothing, [])
        simpleRead x@(')' : _) = return (Nothing, x)
        simpleRead ('(' : t) =
            do (elems, t') <- readElems [] t
               t'' <- case t' of
                   ')' : t'' -> return t''
                   _ -> fail ("SExpression.readSExpr: expecting a string starting with ')' but got " ++ show t' ++ " when parsing " ++ show x)
               return (Just (List elems), t'')
        simpleRead ('"' : t) =
            do (s, t') <- finishString [] t
               return (Just (String s), t')
        simpleRead  (c : t)
          | elem c atomChars =
                let (a, t') = span (flip elem atomChars) t in
                return (Just (Atom (c : a)), t')
          | True = fail ("simpleRead: Unrecognized character: " ++ show c)

        finishString :: Monad m => String -> String -> m (String, String)
        finishString acc ('"' : t) = return (reverse acc, t)
        finishString acc ('\\' : c : t) = finishString (c : acc) t
        finishString _acc "\\" = fail "finishString: lone backslash"
        finishString acc (h : t) = finishString (h : acc) t
        finishString acc [] = fail ("finishString: unexpected empty string; reverse acc = " ++ reverse acc)

        readElems :: Monad m => [Value] -> String -> m ([Value], String)
        readElems acc x =
            do (h, s) <- simpleRead (dropWhile (flip elem whitespace) x)
               case h of
                   Nothing -> return (reverse acc, s)
                   Just e -> readElems (e : acc) s
    in
    do (v, s) <- simpleRead (dropWhile (flip elem whitespace) x)
       case (v, s) of
           (Nothing, []) -> fail "readSExpr: empty string"
           (Nothing, ')' : _) -> fail "readSExpr: unexpected right parenthesis"
           (Just x, s) -> return (x, dropWhile (flip elem whitespace) s)

escapeString :: String -> String
escapeString = (>>= (\x -> if elem x "\"\\" then ['\\', x] else [x]))

showsSExpr :: Value -> ShowS
showsSExpr (Atom x) s = x ++ s
showsSExpr (String x) s = '"' : escapeString x ++ '"' : s
showsSExpr (List x) s = '(' : concatS (intersperse (' ' :) (map showsSExpr x)) (')' : s)

concatS :: [ShowS] -> ShowS
concatS [] s = s
concatS (h : t) s = h (concatS t s)

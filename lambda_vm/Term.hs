module Term (Term(..), ExtImpl(..), reduce) where

data Term external =
    -- A variable. 0 = closest lambda, 1 = outside that, ...
    Var Int
  | Lambda (Term external)
  | Apply (Term external) (Term external)
  | External external
  deriving (Eq, Show)

data ExtImpl ext =
  ExtImpl
  { -- How to apply an ext to term. Must make progress.
    --
    -- TODO: Maybe hide the Term type here so the ExtImpl can't inspect the
    -- term. I'm not sure the ExtImpl should be able to.
    applyExtTerm :: ext -> Term ext -> Term ext
  }

-- Applies beta reduction or the external function until the term is Var,
-- Lambda, or External, or is Apply f x where:
-- (a) f is reduced (in the sense done by this function),
-- (b) f is not Lambda; and
-- (c) f and x aren't both External.
reduce :: ExtImpl ext -> Term ext -> Term ext
reduce _ t@(Var _) = t
reduce _ t@(Lambda _) = t
reduce _ t@(External _) = t
reduce extImpl (Apply f x) =
  case reduce extImpl f of
    t@(Var _) -> Apply t x
    t@(Apply _ _) -> Apply t x
    Lambda b -> reduce extImpl (substitute 0 x b)
    External e -> reduce extImpl (applyExtTerm extImpl e x)

substitute :: Int -> Term ext -> Term ext -> Term ext
substitute depth value (Var d) =
  case compare d depth of
    LT -> Var d
    EQ -> addToDepth depth 0 value
    GT -> Var (d - 1)
substitute depth value (Lambda t) = Lambda (substitute (depth+1) value t)
substitute depth value (Apply f x) =
  Apply (substitute depth value f) (substitute depth value x)
substitute depth _ t@(External _) = t

addToDepth :: Int -> Int -> Term ext -> Term ext
addToDepth a threshold (Var b) = if b < threshold then Var b else Var (a+b)
addToDepth a threshold (Lambda x) = Lambda (addToDepth a (threshold+1) x)
addToDepth a threshold (Apply f x) = Apply (addToDepth a threshold f) (addToDepth a threshold x)
addToDepth _ _ t@(External _) = t

-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module AllTests where

import {-@ HTF_TESTS @-} TermTest hiding (main)
import Test.Framework

main :: IO ()
main = htfMain htf_importedTests

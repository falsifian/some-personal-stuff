module Nat where

data NatOrSuccessor = Nat Int | Successor

applyNatOrSucc :: Maybe NatOrSuccessor -> Term (Maybe NatOrSuccessor) -> Term (Maybe NatOrSuccessor)
applyNatOrSucc Nothing _ = External Nothing
applyNatOrSucc (Just (Nat _)) _ = External Nothing
applyNatOrSucc (Just Successor) t =
  let t' = reduce applyNatOrSucc t in
  case t' of
    External (Just (Nat x)) -> External (Just (Nat (succ x)))
    _ -> External Nothing
applyNatOrSucc a b =
  do f <- a
     case f b of
       

     x <- b
     case f x of
       Successor (Nat n) -> return Nat (succ n)
       _ _ -> Nothing

module Main where

import System.Environment (getArgs)

main :: IO ()
main = do [programPath] <- getArgs
          programText <- readFile programPath
          run (parseTerm programText)

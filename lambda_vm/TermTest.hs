-- Enable the preprocessor for HTF, the Haskell Test Framework.
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module TermTest where

import Term
import Test.Framework

constExtImpl :: a -> ExtImpl a
constExtImpl x = ExtImpl (\_ _ -> External x)

trivialExtImpl :: ExtImpl ()
trivialExtImpl = constExtImpl ()

test_application :: IO ()
test_application =
  do assertEqual (Var 42) (reduce trivialExtImpl (Apply (Lambda (Var 0)) (Var 42)))
     assertEqual (Apply (Var 42) (Var 99)) (reduce trivialExtImpl (Apply (Lambda (Apply (Var 0) (Var 100))) (Var 42)))
     assertEqual (Lambda (Var 43)) (reduce trivialExtImpl (Apply (Lambda (Lambda (Var 1))) (Var 42)))
     assertEqual (Var 5) (reduce trivialExtImpl (Apply (Lambda (Var 6)) (Var 42)))
     assertEqual (Lambda (Lambda (Lambda (Var 1)))) $ reduce trivialExtImpl $
         Apply (Lambda (Lambda (Var 1))) (Lambda (Lambda (Var 1)))
     assertEqual (Var 20) $ reduce trivialExtImpl $
         (Apply (Apply (Apply (Lambda (Lambda (Lambda (Var 1)))) (Var 10))
                       (Var 20))
                (Var 30))

test_doNothing :: IO ()
test_doNothing =
  let assertUnchanged x = assertEqual x (reduce trivialExtImpl x) in
  do assertUnchanged (Var 0)
     assertUnchanged (Lambda (Var 0))
     -- The right side of the Apply can be reduced, but it shouldn't be.
     assertUnchanged (Apply (Var 0) (Apply (Lambda (Var 0)) (Var 0)))
     assertEqual (External "Hello") (reduce (constExtImpl "Goodbye") (External "Hello"))

test_external :: IO ()
test_external =
  let extImpl = ExtImpl (\x y -> case (reduce extImpl y) of
          External y' -> External (x+y')
          t -> Apply t t) in
  do assertEqual (External 7) (reduce extImpl (Apply (External 3) (External 4)))
     assertEqual (Apply (Apply (Var 5) (Var 5)) (Var 0)) $
         reduce extImpl $
         Apply (Apply (External 0) (Var 5)) (Var 0)

-- For test_nontrivial. A simple implementation of natural numbers that, for
-- simplicity, doesn't allow them to be destructed: any attempt to apply an
-- OpaqueNat results in Fail.
data OpaqueNat = OpaqueNat Int | OpaqueSucc | Fail String deriving (Eq, Show)

opaqueNatImpl :: ExtImpl OpaqueNat
opaqueNatImpl =
  let applyOpaqueNat (OpaqueNat n) x = External (Fail ("Apply " ++ show n ++ " to " ++ show x))
      applyOpaqueNat OpaqueSucc t =
        case reduce opaqueNatImpl t of
          External (OpaqueNat n) -> External (OpaqueNat (succ n))
          x -> External (Fail ("successor of " ++ show x))
      applyOpaqueNat f@(Fail _) _ = External f
  in ExtImpl applyOpaqueNat

-- To make some non-trivial test cases, let's try some arithmetic with Church
-- numerals.
test_churchArithmetic :: IO ()
test_churchArithmetic =
  let churchNumeral n = Lambda (Lambda (churchNumeralBody n))
      churchNumeralBody n =
        if n == 0 then Var 1 else Apply (Var 0) (churchNumeralBody (n-1))
  in
  do -- Evaluate a Church numeral.
     assertEqual (External (OpaqueNat 4)) $ reduce opaqueNatImpl $
       Apply (Apply (churchNumeral 4) (External (OpaqueNat 0))) (External OpaqueSucc)
     -- Add 2+7 with lambda calculus.
     assertEqual (External (OpaqueNat 9)) $ reduce opaqueNatImpl $
         Apply (Apply (Apply (Apply
             (Lambda (Lambda (Lambda (Lambda
               -- Var 3: summand
               -- Var 2: summand
               -- Var 1: 0
               -- Var 0: successor
               (Apply (Apply (Var 2) (Apply (Apply (Var 3) (Var 1)) (Var 0))) (Var 0))))))
             (churchNumeral 2))
           (churchNumeral 7)) (External (OpaqueNat 0))) (External OpaqueSucc)
     -- Multiply 5 * 7 with lambda calculus.
     assertEqual (External (OpaqueNat 35)) $ reduce opaqueNatImpl $
         Apply (Apply (Apply (Apply
             -- Multiply
             (Lambda (Lambda (Lambda (Lambda
               -- Var 3: multiplicand
               -- Var 2: multiplicand
               -- Var 1: 0
               -- Var 0: successor
               (Apply (Apply (Var 2) (Var 1))
                      -- Add Var 3.
                      (Lambda (Apply (Apply (Var 4) (Var 0)) (Var 1))))))))
             (churchNumeral 5)) (churchNumeral 7)) (External (OpaqueNat 0))) (External OpaqueSucc)

main :: IO ()
main = htfMain htf_thisModulesTests

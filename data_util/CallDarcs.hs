module CallDarcs (darcsAddCommit, darcsCheckNoNew) where

import Control.Monad.Error.Class
import qualified Data.ByteString.UTF8 as BU8
import System.Exit
import System.FilePath
import System.IO
import System.IO.Temp
import System.Process
import Text.Printf
import qualified Text.ShellEscape as Esc

-- Add a file and commit.
darcsAddCommit :: FilePath -> FilePath -> String -> IO ()
darcsAddCommit repoPath newFilePath commitMsg = withStringInTempFile commitMsg $ \commitMsgFilePath ->
  do exitCode <- simpleRun repoPath $ printf "darcs add %s" (escapeStringForShell newFilePath)
     case exitCode of
       ExitSuccess -> return ()
       ExitFailure code -> throwError $ strMsg $ printf "error adding to darcs repo: exit code %d" code
     exitCode' <- simpleRun repoPath $ printf "darcs record -a --logfile=%s" commitMsgFilePath
     case exitCode' of
       ExitSuccess -> return ()
       ExitFailure code -> throwError $ strMsg $ printf "error committing to darcs repo: exit code %d" code

-- Make sure there are no pending changes in a darcs repository.
darcsCheckNoNew :: FilePath -> IO ()
darcsCheckNoNew repoPath =
  do exitCode <- simpleRun repoPath "darcs whatsnew -l"
     case exitCode of
       ExitSuccess -> throwError $ strMsg $ printf "deal with pending changes in the darcs repo at %s first" (show repoPath)
       ExitFailure _code -> return ()

escapeStringForShell :: String -> String
escapeStringForShell s = BU8.toString (Esc.bytes (Esc.escape (BU8.fromString s) :: Esc.Sh))

simpleRun :: FilePath -> String -> IO ExitCode
simpleRun cwd cmd =
  do (_, _, _, h) <- createProcess (shell cmd) {cwd = Just cwd, close_fds = True}
     waitForProcess h

withStringInTempFile :: String -> (FilePath -> IO a) -> IO a
withStringInTempFile content io =
  withSystemTempFile "temp-" $ \path handle ->
    do hPutStr handle $ content
       hClose handle
       io path

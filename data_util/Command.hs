module Command (Command(..), standardUsage, getOptCmd) where

import Control.Monad
import System.Console.GetOpt
import System.Exit
import System.IO
import Text.Printf

data Command =
  Cmd
  { action :: [String] -> IO () -- Takes arguments as input, including the name by which it was called.
  , usage :: String -> String  -- Takes the name by which it is called as input.
  }

standardUsage :: [OptDescr a] -> Maybe String -> String -> String
standardUsage optionDescrs msg name = maybe id (\m m' -> m ++ "\n" ++ m') msg $ usageInfo ("comand: " ++ name) optionDescrs

getOptCmd :: o -> [OptDescr (o -> o)] -> (o -> String -> [String] -> IO ()) -> [String] -> IO ()
getOptCmd defaultOptions optionDescrs main (cmdName : args) =
  do let (optChanges, extraArgs, errors) = getOpt RequireOrder optionDescrs args
     unless (null errors) $ 
       do hPutStr stderr
            ( standardUsage
              optionDescrs
              ( Just $ printf "Error(s) parsing arguments:\n%s" $
                unlines $ errors
              )
              cmdName
            )
          exitWith (ExitFailure (-1))
     main (foldl (flip ($)) defaultOptions optChanges) cmdName extraArgs

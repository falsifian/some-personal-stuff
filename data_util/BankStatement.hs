module BankStatement (BankStatementInfo(..)) where

import Control.Monad
import MoneyTracking.Log (DateRange, readDateRange, showDateRange)
import Text.SExpression as SE
import Text.SExpression.Format

data BankStatementInfo =
  BankStatementInfo
  { biImportRecordRefs :: ![String]
  , biTransactions :: !(Maybe [String])
  , biDateRange :: !(Maybe DateRange)
  , biPools :: !(Maybe [String])
  }

instance SExprFormat BankStatementInfo where
  sExprFormat =
    ( 0
    , [ ("import-record-refs", Field {fOptional = False, fGet = Just . map SE.Atom . biImportRecordRefs})
      , ("transactions", Field {fOptional = True, fGet = liftM (map SE.Atom) biTransactions})
      , ("dates", Field {fOptional = True, fGet = liftM showDateRange biDateRange})
      , ("pools", Field {fOptional = True, fGet = liftM (map SE.Atom) biPools})
      ]
    )
    getInitialFields = const []
    consFromFormat [] attribs =
      do irrSEs <- lookupSE "import-record-refs" attribs
         irr <- sequence
           [ case el of
                SE.Atom s -> return s
                _ -> fail $ printf "expected an atom but didn't get one: %s" (show el)
           | el <- irrSEs
           ]
         transactions <- liftM (mapM readSingleAtom) $ Map.lookup "transactions" attribs
         dates <- liftM readDateRange $ Map.lookup "dates" attribs
         pools <- liftM (mapM readSingleAtom) $ Map.lookup "pool" attribs
         return $
           BankStatementInfo
           { biImportRecordRefs = irr
           , biTransactions = transactions
           , biDateRange = dates
           , biPools = pools
           }

readSingleAtom :: Monad m => SE.Value -> m String
readSingleAtom [SE.Atom t] = t
readSingleAtom x = fail $ printf "Expected a single atom; got %s" (show x)

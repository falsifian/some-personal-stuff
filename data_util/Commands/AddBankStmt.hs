-- Commands.AddBankStmt -- add a bank statement

module Commands.AddBankStmt (command) where

import BankStatement
import CallDarcs
import qualified Codec.Binary.DataEncoding as DE
import Command
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as BU8
import Data.Time.Clock (getCurrentTime)
import ImportRecord
import qualified MTree.MTree as MTree
import qualified MTree.CAS
import MoneyTracking.Log (DateRange, readDateRange)
import Network.BSD (getHostName)
import System.Console.GetOpt
import System.Directory (getCurrentDirectory)
import System.Environment (getProgName)
import Util

command :: Command
command = Cmd { action = getOptCmd defaultOptions optionDescrs main
              , usage = standardUsage optionDescrs Nothing
              }

main :: Options -> String -> [String] -> IO ()
main options cmdName statementFilePaths =
  do casPath <- fromNothingErr (standardUsage optionDescrs (Just "CAS path") cmdName) (optCasPath options)
     recPath <- fromNothingErr (standardUsage optionDescrs (Just "rec path") cmdName) (optRecPath options)
     stmtFilePath <- fromNothingErr (standardUsage optionDescrs (Just "statement path") cmdName) (optRecPath options)
     darcsCheckNoNew recPath
     hostName <- getHostName
     userName <- getUserName
     date <- getCurrentTime
     progName <- getProgName
     cwd <- getCurrentDirectory
     stmtData <- B.readFile stmtFilePath
     let importRecord = BU8.fromString $ show $ SE.List $ showFormatted $
         ImportRecord
         { irImportDate = Just date
         , irImportHostName = Just hostName
         , irImportPath = Just (cwd </> stmtFilePath)
         , irImportUserName = Just userName
         }
     dataNode <- MTree.construct stmtData []
     importRecordNode <- MTree.construct importRecord [dataNode]
     let importRecordNodeHash = MTree.CAS.getHash importRecordNode

     stmtID <- randomID
     let stmtRecPath = recPath </> "bank_stmts" </> stmtID
     doesFileExist stmtRecPath >>= flip when (fail "The record name I chose is already taken.  This shouldn't happen.")
     stmtRecHandle <-
       openFd
         stmtRecPath
         System.Posix.IO.WriteOnly
         (Just 0o444)
         (System.Posix.defaultFileFlags {exclusive = True})
       >>= fdToHandle
     hPrint stmtRecHandle $ SE.List $ showFormatted $
       BankStatementInfo
       { biImportRecordRefs = ["casdag_sha256:" ++ DE.encode DE.base64 importRecordNodeHash]
       , biTransactions = optTransactions options
       , biDateRange = optDateRange options
       , biPools = optPools options
       }

data Options =
  Options
  { optCasPath :: !(Maybe FilePath)
  , optRecPath :: !(Maybe FilePath)
  , optStmtPath :: !(Maybe FilePath)
  , optTransactions :: !(Maybe [String])
  , optDateRange :: !(Maybe DateRange)
  , optPools :: !(Maybe [String])
  }

-- TODO:  Add switches for all the options.
optionDescrs :: [OptDescr (Options -> Options)]
optionDescr =
  [ Option "c" []
    (ReqArg (\s o -> o {optCasPath = Just s}) "CAS path")
    "CAS path"
  , Option "r" []
    (ReqArg (\s o -> o {optRecPath = Just s}) "rec path")
    "rec path"
  ]

defaultOptions :: Options
defaultOptions =
  Options
  { optCasPath = Nothing
  , optRecPath = Nothing
  }

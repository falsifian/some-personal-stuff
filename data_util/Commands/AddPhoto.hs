-- Commands.AddPhoto -- add a photo

module Commands.AddPhoto (command) where

import qualified CAS
import qualified CAS.Hash
import CallDarcs
import qualified Codec.Binary.DataEncoding as DE
import Command
import Control.Monad
import qualified Data.ByteString as BS
import Data.Time.Clock
import Network.BSD (getHostName)
import Photo
import System.Console.GetOpt
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO
import System.Posix.Files
import System.Posix.IO
import Text.Printf
import Text.SExpression as SE
import Text.SExpression.Format
import Util

data Options =
  Options
  { optCameraDesc :: !(Maybe String)
  , optCasPath :: !(Maybe FilePath)
  , optRecPath :: !(Maybe FilePath)
  , optSymlinkPath :: !(Maybe FilePath)
  }
  
defaultOptions :: Options
defaultOptions =
  Options
  { optCameraDesc = Nothing
  , optCasPath = Nothing
  , optRecPath = Nothing
  , optSymlinkPath = Nothing
  }

optionDescrs :: [OptDescr (Options -> Options)]
optionDescrs =
  [ Option "c" []
    (ReqArg (\s o -> o {optCasPath = Just s}) "CAS path")
    "CAS path"
  , Option "d" []
    (ReqArg (\s o -> o {optCameraDesc = Just s}) "free-form camera description")
    "A free-form description of the camera that took the photo."
  , Option "r" []
    (ReqArg (\s o -> o {optRecPath = Just s}) "rec path")
    "rec path"
  , Option "s" []
    (ReqArg (\s o -> o {optSymlinkPath = Just s}) "symlink dir path")
    "symlink dir path"
  ]

main :: Options -> String -> [String] -> IO ()
main options cmdName photoFilePaths =
  do let cameraDesc = optCameraDesc options
     casPath <- fromNothingErr (standardUsage optionDescrs (Just "CAS path") cmdName) (optCasPath options)
     recPath <- fromNothingErr (standardUsage optionDescrs (Just "rec path") cmdName) (optRecPath options)
     darcsCheckNoNew recPath
     hostName <- getHostName
     userName <- getUserName
     date <- getCurrentTime
     progName <- getProgName
     cwd <- getCurrentDirectory
     sequence
       [ do printf "Adding %s...\n" (show photoFilePath)
            photoData <- BS.readFile photoFilePath
            (hash, _existed) <- CAS.run (CAS.add photoData) casPath
            photoID <- randomID
            recordID <- randomID
            let photoRecDirPath = "photos" </> photoID
            createDirectory $ recPath </> photoRecDirPath
            darcsAddCommit recPath photoRecDirPath $ printf "host:%s type:dir-add user:%s" hostName userName
            let photoRecordPath = photoRecDirPath </> recordID
            doesFileExist (recPath </> photoRecordPath) >>= flip when (fail "Strange, the record name I chose is already taken.  The probability of this happening is negligable, so this is probably a bug.")
            photoRecHandle <-
              openFd
              (recPath </> photoRecordPath)
              System.Posix.IO.WriteOnly
              (Just 0o444)
              (System.Posix.IO.defaultFileFlags {exclusive = True})
              >>= fdToHandle
            hPrint photoRecHandle $ SE.List $ showFormatted $
              PhotoInfo
              { piCameraDesc = cameraDesc
              , piHashes = ["sha256:" ++ DE.encode DE.base64 (CAS.Hash.sha256ToBytes hash)]
              , piImportDate = date
              , piImportHostName = hostName
              , piImportPath = cwd </> photoFilePath
              , piImportUserName = userName
              }
            hClose photoRecHandle
            darcsAddCommit recPath photoRecordPath $ printf "host:%s type:file-add user:%s" hostName userName
            printf "    %s\n" (photoID </> recordID)
            case optSymlinkPath options of
              Nothing -> return ()
              Just p -> addPhotoSymlink p casPath photoID recordID hash
       | photoFilePath <- photoFilePaths
       ]
     putStrLn $ printf "%s: success" progName

addPhotoSymlink :: FilePath -> FilePath -> String -> String -> CAS.Hash.SHA256 -> IO ()
addPhotoSymlink symlinkPath casPath photoID recordID hash =
  do cwd <- getCurrentDirectory
     createSymbolicLink
       (cwd </> casPath </> "data" </> CAS.Hash.sha256ToHex hash)  -- TODO: the code to get this path should really be in CAS.
       (symlinkPath </> (photoID ++ "-" ++ recordID))

command :: Command
command = Cmd { action = getOptCmd defaultOptions optionDescrs main
              , usage = standardUsage optionDescrs Nothing
              }

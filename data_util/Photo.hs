module Photo (PhotoInfo(..), getAllPhotos, getPhotoSHA256) where

import qualified CAS.Hash
import Control.Exception
import Control.Monad
import Control.Monad.Error.Class
import qualified Data.Map as Map
import Data.Time.Clock
import Data.Time.Format
import System.Directory
import System.FilePath
import System.IO.Error (isDoesNotExistError)
import Text.Printf
import Text.SExpression as SE
import Text.SExpression.Format
import Util

data PhotoInfo =
  PhotoInfo  -- A lot of these fields could be change to Maybes.
  { piCameraDesc :: !(Maybe String)
  , piHashes :: ![String]
  , piImportDate :: !UTCTime
  , piImportHostName :: !String
  , piImportPath :: !FilePath
  , piImportUserName :: !String
  }

-- getAllPhotos recPath returns a list with information about each photo listed
-- under (recPath)/photos.  The information about a photo is a tuple (photoID,
-- recordID, photoInfo).
getAllPhotos :: String -> IO [(String, String, PhotoInfo)]
getAllPhotos recPath =
  do photos <- handleJust (guard . isDoesNotExistError) (const $ return []) $
                   getDirectoryContents (recPath </> "photos")
     sequence
       [ do let photoPath = recPath </> "photos" </> photoID
            recordIDs <- liftM (filter (\x -> not (elem x [".", ".."]))) $ getDirectoryContents photoPath  -- TODO: allow for more than one photo instance
            recordID <- case recordIDs of
              [r] -> return r
              _ -> throwError $ strMsg $ printf "Wrong number of photo records at %s: %s" photoPath (show recordIDs)
            (SE.List photoInfoSE, "") <- (readFile $ photoPath </> recordID) >>= readSExpr
            photoInfo <- readFormatted photoInfoSE
            return (photoID, recordID, photoInfo)
         | photoID <- photos
         , not (elem photoID [".", ".."])
         ]

getPhotoSHA256 :: (Error e, MonadError e m) => PhotoInfo -> m CAS.Hash.SHA256
getPhotoSHA256 descr = getSHA256FromHashList (piHashes descr)

instance SExprFormat PhotoInfo where
  sExprFormat =
    ( 0,
      [ ("camera-desc", maybeStrField piCameraDesc)
      , ("hashes", Field {fOptional = False, fGet = Just . map SE.Atom . piHashes})
      , ("import-date", reqAtomField (dateToString . piImportDate))
      , ("import-hostname", reqStrField piImportHostName)
      , ("import-path", reqStrField piImportPath)
      , ("import-user-name", reqStrField piImportUserName)
      ]
    )
  getInitialFields = const []
  consFromFormat [] attribs =
    do cd <- case Map.lookup "camera-desc" attribs of
         Nothing -> return Nothing
         Just [SE.String desc] -> return (Just desc)
         _ -> fail "Bad camera-desc in PhotoInfo s-expression."
       Just hashes <- lookupAtomList "hashes" attribs
       Just idts <- lookupAtom "import-date" attribs
       idt <- maybe
              (fail $ printf "badly formatted date: %s; attribs: %s" (show idts) (show attribs))
              return
              (stringToDate idts)
       Just ihn <- lookupString "import-hostname" attribs
       Just ipa <- lookupString "import-path" attribs
       Just iun <- lookupString "import-user-name" attribs
       return $
         PhotoInfo
         { piCameraDesc = cd
         , piHashes = hashes
         , piImportDate = idt
         , piImportHostName = ihn
         , piImportPath = ipa
         , piImportUserName = iun
         }

timeFormat :: String
timeFormat = "%Y-%m%d%H%M%S.%q"

dateToString :: UTCTime -> String
dateToString = formatTime defaultTimeLocale timeFormat

stringToDate :: String -> Maybe UTCTime
stringToDate = parseTime defaultTimeLocale timeFormat

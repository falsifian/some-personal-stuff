module ImportRecord
( ImportRecord(..)
, getAllImportRecords
, getImportRecordSHA256
) where

import CAS.Hash
import Control.Exception (handleJust)
import Control.Monad
import Control.Monad.Error
import Data.Time.Clock (UTCTime)
import Data.Time.Format
import System.Directory
import System.FilePath
import System.IO.Error (isDoesNotExistError)
import Text.Printf
import Text.SExpression as SE
import Text.SExpression.Format
import Util

data ImportRecord =
  ImportRecord
  { irHashes :: ![String]
  , irImportDate :: !(Maybe UTCTime)
  , irImportHostName :: !(Maybe String)
  , irImportPath :: !(Maybe String)
  , irImportUserName :: !(Maybe String)
  }

instance SExprFormat ImportRecord where
  sExprFormat =
    ( 0
    , [ ("hashes", Field {fOptional = False, fGet = Just . map SE.Atom . irHashes})
      , ("import-date", maybeAtomField (liftM dateToString . irImportDate))
      , ("import-hostname", maybeStrField irImportHostName)
      , ("import-path", maybeStrField irImportPath)
      , ("import-user-name", maybeStrField irImportUserName)
      ]
    )
  getInitialFields = const []
  consFromFormat [] attribs =
    do Just hashes <- lookupAtomList "hashes" attribs
       idts <- lookupAtom "import-date" attribs
       idt <- maybe (return Nothing) (liftM Just . stringToDate) idts
       ihn <- lookupString "import-hostname" attribs
       ipa <- lookupString "import-path" attribs
       iun <- lookupString "import-user-name" attribs
       return $
         ImportRecord
         { irHashes = hashes
         , irImportDate = idt
         , irImportHostName = ihn
         , irImportPath = ipa
         , irImportUserName = iun
         }

timeFormat :: String
timeFormat = iso8601DateFormat (Just "%H%M")

dateToString :: UTCTime -> String
dateToString = formatTime defaultTimeLocale timeFormat

stringToDate :: (Error e, MonadError e m) => String -> m UTCTime
stringToDate s =
    case parseTime defaultTimeLocale timeFormat s of
        Nothing -> throwError $ strMsg $ printf "badly formatted date: %s" (show s)
        Just d -> return d

-- getAllImportRecords irPath returns a list with information about each import
-- record listed under irPath.  The information about an import record is a
-- tuple (file name, record of type ImportRecord).
getAllImportRecords :: String -> IO [(String, ImportRecord)]
getAllImportRecords dirPath =
  do fileNames <- handleJust (guard . isDoesNotExistError) (const $ return []) $
                      getDirectoryContents dirPath
     sequence
       [ do let irPath = dirPath </> fileName
            (SE.List importRecordSE, "") <- (readFile $ irPath)
                                            >>= readSExpr
            importRecord <- readFormatted importRecordSE
            return (fileName, importRecord)
         | fileName <- fileNames
         , not (elem fileName [".", ".."])
         ]

getImportRecordSHA256 :: (Error e, MonadError e m) => ImportRecord -> m CAS.Hash.SHA256
getImportRecordSHA256 descr = getSHA256FromHashList (irHashes descr)

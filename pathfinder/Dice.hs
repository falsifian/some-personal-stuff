module Dice where

import System.Random
import Text.Printf (printf)

d :: Int -> IO Int
d n = do x <- randomRIO (1, n)
         printf "roll: %d/%d\n" x n
         return x

ds :: [Int] -> Int -> IO Int
ds ns m = do xs <- sequence (map d ns)
             let result = sum xs + m
             printf "+ %d = %d\n" m result
             return result

d20 :: Int -> IO Int
d20 m = ds [20] m
